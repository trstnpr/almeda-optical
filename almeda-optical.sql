-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Jan 13, 2017 at 10:36 AM
-- Server version: 10.1.9-MariaDB
-- PHP Version: 5.6.15

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `almeda-optical`
--

-- --------------------------------------------------------

--
-- Table structure for table `appointments`
--

CREATE TABLE `appointments` (
  `id` int(10) UNSIGNED NOT NULL,
  `type` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `notes` varchar(1000) COLLATE utf8_unicode_ci NOT NULL,
  `due_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `patient_id` int(10) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `appointments`
--

INSERT INTO `appointments` (`id`, `type`, `notes`, `due_at`, `created_at`, `updated_at`, `patient_id`) VALUES
(24, 'online', 'Lorem ipsum', '2016-09-23 02:00:00', '2016-09-20 09:21:09', '2016-09-20 09:21:09', 26),
(26, 'online', 'Lorem', '2016-09-21 01:00:00', '2016-09-20 09:23:34', '2016-09-20 09:23:34', 28),
(28, 'online', 'Lorem ipsum dolor sit amet', '2016-09-22 02:00:00', '2016-09-20 09:24:36', '2016-09-20 09:24:36', 30),
(30, 'online', 'Lorem', '2016-09-26 01:00:00', '2016-09-20 09:30:41', '2016-09-20 09:30:41', 32),
(31, 'online', 'Lorem ipsum', '2016-10-01 03:00:00', '2016-09-20 09:31:41', '2016-09-20 09:31:41', 33),
(32, 'online', 'Lorem', '2016-11-01 02:00:00', '2016-09-20 09:36:28', '2016-09-20 09:36:28', 34),
(33, 'online', 'sadfsadf', '2016-11-22 01:00:00', '2016-09-20 09:37:12', '2016-09-20 09:37:12', 35);

-- --------------------------------------------------------

--
-- Table structure for table `items`
--

CREATE TABLE `items` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `price` double NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `item_order`
--

CREATE TABLE `item_order` (
  `item_id` int(10) UNSIGNED NOT NULL,
  `order_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `logs`
--

CREATE TABLE `logs` (
  `id` int(10) UNSIGNED NOT NULL,
  `eye_grade_left` double NOT NULL,
  `eye_grade_right` double NOT NULL,
  `comments` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `appointment_id` int(10) UNSIGNED DEFAULT NULL,
  `patient_id` int(10) UNSIGNED DEFAULT NULL,
  `user_id` int(10) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`migration`, `batch`) VALUES
('2014_10_12_000000_create_users_table', 1),
('2014_10_12_100000_create_password_resets_table', 1),
('2016_09_01_110206_create_roles_table', 1),
('2016_09_01_110214_create_permissions_table', 1),
('2016_09_01_110226_create_patients_table', 1),
('2016_09_01_110227_create_appointments_table', 1),
('2016_09_01_110237_create_logs_table', 1),
('2016_09_01_110312_create_items_table', 1),
('2016_09_01_110327_create_orders_table', 1),
('2016_09_01_110446_create_role_user_table', 1),
('2016_09_01_110457_create_permission_role_table', 1),
('2016_09_01_110508_create_item_order_table', 1);

-- --------------------------------------------------------

--
-- Table structure for table `orders`
--

CREATE TABLE `orders` (
  `id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `patient_id` int(10) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `patients`
--

CREATE TABLE `patients` (
  `id` int(10) UNSIGNED NOT NULL,
  `last_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `first_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `middle_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `address_line_1` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `address_line_2` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `address_city` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `address_state` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `address_zipcode` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `cell_number` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `patients`
--

INSERT INTO `patients` (`id`, `last_name`, `first_name`, `middle_name`, `address_line_1`, `address_line_2`, `address_city`, `address_state`, `address_zipcode`, `cell_number`, `email`, `created_at`, `updated_at`) VALUES
(26, 'Sheeran', 'Ed', 'Es', '#123 Maes Street', 'Barangay', 'Makati City', 'Metro Manila', '2222', '09876543211', 'admin@op.devhub.ph', '2016-09-20 09:21:09', '2016-09-20 09:21:09'),
(27, 'Sheeran', 'Ed', 'Es', '#123 Maes Street', 'Barangay', 'Makati City', 'Metro Manila', '2222', '09876543211', 'admin@op.devhub.ph', '2016-09-20 09:21:09', '2016-09-20 09:21:09'),
(28, 'Gilchrist', 'Michael', 'Kidd', '#123 Maes Street', 'Barangay', 'Makati City', 'Metro Manila', '2222', '09876543211', 'admin@op.devhub.ph', '2016-09-20 09:23:34', '2016-09-20 09:23:34'),
(29, 'Gilchrist', 'Michael', 'Kidd', '#123 Maes Street', 'Barangay', 'Makati City', 'Metro Manila', '2222', '09876543211', 'admin@op.devhub.ph', '2016-09-20 09:23:34', '2016-09-20 09:23:34'),
(30, 'Johnson', 'Dwane', 'The Rock', '#123 Maes Street', 'Barangay', 'Makati City', 'Metro Manila', '2222', '09876543211', 'admin@op.devhub.ph', '2016-09-20 09:24:36', '2016-09-20 09:24:36'),
(31, 'Johnson', 'Dwane', 'The Rock', '#123 Maes Street', 'Barangay', 'Makati City', 'Metro Manila', '2222', '09876543211', 'admin@op.devhub.ph', '2016-09-20 09:24:36', '2016-09-20 09:24:36'),
(32, 'Cruz', 'Juan', 'Dela', '#123 Maes Street', 'Barangay', 'Makati City', 'Metro Manila', '2222', '09876543211', 'admin@op.devhub.ph', '2016-09-20 09:30:41', '2016-09-20 09:30:41'),
(33, 'Grande', 'Arriana', 'Arriana', '#123 Maes Street', 'Barangay', 'Makati City', 'Metro Manila', '2222', '09876543211', 'admin@op.devhub.ph', '2016-09-20 09:31:41', '2016-09-20 09:31:41'),
(34, 'Cruz', 'Dwane', 'Kidd', '#123 Maes Street', 'Barangay', 'Makati City', 'Metro Manila', '2222', '09876543211', 'admin@op.devhub.ph', '2016-09-20 09:36:28', '2016-09-20 09:36:28'),
(35, 'Grande', 'Dwane', 'Arriana', '#123 Maes Street', 'Barangay', 'Makati City', 'Metro Manila', '2222', '09876543211', 'admin@op.devhub.ph', '2016-09-20 09:37:12', '2016-09-20 09:37:12');

-- --------------------------------------------------------

--
-- Table structure for table `permissions`
--

CREATE TABLE `permissions` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `permissions`
--

INSERT INTO `permissions` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'Create Roles', '2016-09-06 22:26:57', '2016-09-06 22:26:57'),
(2, 'Read Roles', '2016-09-06 22:26:57', '2016-09-06 22:26:57'),
(3, 'Update Roles', '2016-09-06 22:26:57', '2016-09-06 22:26:57'),
(4, 'Delete Roles', '2016-09-06 22:26:57', '2016-09-06 22:26:57'),
(5, 'Create Users', '2016-09-06 22:26:57', '2016-09-06 22:26:57'),
(6, 'Read Users', '2016-09-06 22:26:57', '2016-09-06 22:26:57'),
(7, 'Update Users', '2016-09-06 22:26:57', '2016-09-06 22:26:57'),
(8, 'Delete Users', '2016-09-06 22:26:57', '2016-09-06 22:26:57'),
(9, 'Create Appointments', '2016-09-06 22:26:57', '2016-09-06 22:26:57'),
(10, 'Read Appointments', '2016-09-06 22:26:57', '2016-09-06 22:26:57'),
(11, 'Update Appointments', '2016-09-06 22:26:57', '2016-09-06 22:26:57'),
(12, 'Delete Appointments', '2016-09-06 22:26:57', '2016-09-06 22:26:57'),
(13, 'Create Logs', '2016-09-06 22:26:57', '2016-09-06 22:26:57'),
(14, 'Read Logs', '2016-09-06 22:26:57', '2016-09-06 22:26:57'),
(15, 'Update Logs', '2016-09-06 22:26:57', '2016-09-06 22:26:57'),
(16, 'Delete Logs', '2016-09-06 22:26:57', '2016-09-06 22:26:57'),
(17, 'Create Items', '2016-09-06 22:26:57', '2016-09-06 22:26:57'),
(18, 'Read Items', '2016-09-06 22:26:57', '2016-09-06 22:26:57'),
(19, 'Update Items', '2016-09-06 22:26:57', '2016-09-06 22:26:57'),
(20, 'Delete Items', '2016-09-06 22:26:57', '2016-09-06 22:26:57'),
(21, 'Create Orders', '2016-09-06 22:26:57', '2016-09-06 22:26:57'),
(22, 'Read Orders', '2016-09-06 22:26:57', '2016-09-06 22:26:57'),
(23, 'Update Orders', '2016-09-06 22:26:57', '2016-09-06 22:26:57'),
(24, 'Delete Orders', '2016-09-06 22:26:57', '2016-09-06 22:26:57');

-- --------------------------------------------------------

--
-- Table structure for table `permission_role`
--

CREATE TABLE `permission_role` (
  `permission_id` int(10) UNSIGNED NOT NULL,
  `role_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `permission_role`
--

INSERT INTO `permission_role` (`permission_id`, `role_id`) VALUES
(1, 1),
(2, 1),
(3, 1),
(4, 1),
(5, 1),
(6, 1),
(7, 1),
(8, 1),
(9, 1),
(10, 1),
(11, 1),
(12, 1),
(13, 1),
(14, 1),
(15, 1),
(16, 1),
(17, 1),
(18, 1),
(19, 1),
(20, 1),
(21, 1),
(22, 1),
(23, 1),
(24, 1),
(1, 2),
(2, 2),
(3, 2),
(4, 2),
(5, 2),
(6, 2),
(7, 2),
(8, 2),
(9, 2),
(10, 2),
(11, 2),
(12, 2),
(13, 2),
(14, 2),
(15, 2),
(16, 2),
(17, 2),
(18, 2),
(19, 2),
(20, 2),
(21, 2),
(22, 2),
(23, 2),
(24, 2),
(9, 3),
(10, 3),
(11, 3),
(12, 3),
(13, 3),
(14, 3),
(15, 3),
(16, 3),
(17, 4),
(18, 4),
(19, 4),
(20, 4),
(21, 4),
(22, 4),
(23, 4),
(24, 4);

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE `roles` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'Site Administrator', '2016-09-06 22:26:57', '2016-09-06 22:26:57'),
(2, 'Backend Support', '2016-09-06 22:26:58', '2016-09-06 22:26:58'),
(3, 'Doctor', '2016-09-06 22:26:58', '2016-09-06 22:26:58'),
(4, 'Clerk', '2016-09-06 22:26:58', '2016-09-06 22:26:58');

-- --------------------------------------------------------

--
-- Table structure for table `role_user`
--

CREATE TABLE `role_user` (
  `role_id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `role_user`
--

INSERT INTO `role_user` (`role_id`, `user_id`) VALUES
(1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `username` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `last_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `first_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `middle_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `avatar` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `username`, `email`, `password`, `remember_token`, `last_name`, `first_name`, `middle_name`, `avatar`, `created_at`, `updated_at`) VALUES
(1, 'admin', 'admin@op.devhub.ph', '$2y$10$Il.d9AK6Ri8aJ9Cpbzjv1uBoUwFxl5aixHYm6T9JGRKY4NaDNh3nm', 'g9s1dKLM33aq3vYpoGIjJPsaqszEZv3RlWwWSoJV9g4l5gl4DwezfFRLzFAF', 'Admin', 'Super', 'Duper', '', '2016-09-06 22:26:59', '2016-09-20 09:23:05');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `appointments`
--
ALTER TABLE `appointments`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `appointments_due_at_unique` (`due_at`),
  ADD KEY `appointments_patient_id_foreign` (`patient_id`),
  ADD KEY `appointments_id_index` (`id`);

--
-- Indexes for table `items`
--
ALTER TABLE `items`
  ADD PRIMARY KEY (`id`),
  ADD KEY `items_id_index` (`id`);

--
-- Indexes for table `item_order`
--
ALTER TABLE `item_order`
  ADD KEY `item_order_item_id_index` (`item_id`),
  ADD KEY `item_order_order_id_index` (`order_id`);

--
-- Indexes for table `logs`
--
ALTER TABLE `logs`
  ADD PRIMARY KEY (`id`),
  ADD KEY `logs_appointment_id_foreign` (`appointment_id`),
  ADD KEY `logs_patient_id_foreign` (`patient_id`),
  ADD KEY `logs_user_id_foreign` (`user_id`),
  ADD KEY `logs_id_index` (`id`);

--
-- Indexes for table `orders`
--
ALTER TABLE `orders`
  ADD PRIMARY KEY (`id`),
  ADD KEY `orders_patient_id_foreign` (`patient_id`),
  ADD KEY `orders_id_index` (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`),
  ADD KEY `password_resets_token_index` (`token`);

--
-- Indexes for table `patients`
--
ALTER TABLE `patients`
  ADD PRIMARY KEY (`id`),
  ADD KEY `patients_id_index` (`id`);

--
-- Indexes for table `permissions`
--
ALTER TABLE `permissions`
  ADD PRIMARY KEY (`id`),
  ADD KEY `permissions_id_index` (`id`);

--
-- Indexes for table `permission_role`
--
ALTER TABLE `permission_role`
  ADD KEY `permission_role_permission_id_index` (`permission_id`),
  ADD KEY `permission_role_role_id_index` (`role_id`);

--
-- Indexes for table `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`),
  ADD KEY `roles_id_index` (`id`);

--
-- Indexes for table `role_user`
--
ALTER TABLE `role_user`
  ADD KEY `role_user_role_id_index` (`role_id`),
  ADD KEY `role_user_user_id_index` (`user_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_username_unique` (`username`),
  ADD UNIQUE KEY `users_email_unique` (`email`),
  ADD KEY `users_id_index` (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `appointments`
--
ALTER TABLE `appointments`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=34;
--
-- AUTO_INCREMENT for table `items`
--
ALTER TABLE `items`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `logs`
--
ALTER TABLE `logs`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `orders`
--
ALTER TABLE `orders`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `patients`
--
ALTER TABLE `patients`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=36;
--
-- AUTO_INCREMENT for table `permissions`
--
ALTER TABLE `permissions`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;
--
-- AUTO_INCREMENT for table `roles`
--
ALTER TABLE `roles`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `appointments`
--
ALTER TABLE `appointments`
  ADD CONSTRAINT `appointments_patient_id_foreign` FOREIGN KEY (`patient_id`) REFERENCES `patients` (`id`);

--
-- Constraints for table `item_order`
--
ALTER TABLE `item_order`
  ADD CONSTRAINT `item_order_item_id_foreign` FOREIGN KEY (`item_id`) REFERENCES `items` (`id`),
  ADD CONSTRAINT `item_order_order_id_foreign` FOREIGN KEY (`order_id`) REFERENCES `orders` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `logs`
--
ALTER TABLE `logs`
  ADD CONSTRAINT `logs_appointment_id_foreign` FOREIGN KEY (`appointment_id`) REFERENCES `appointments` (`id`),
  ADD CONSTRAINT `logs_patient_id_foreign` FOREIGN KEY (`patient_id`) REFERENCES `patients` (`id`),
  ADD CONSTRAINT `logs_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`);

--
-- Constraints for table `orders`
--
ALTER TABLE `orders`
  ADD CONSTRAINT `orders_patient_id_foreign` FOREIGN KEY (`patient_id`) REFERENCES `patients` (`id`);

--
-- Constraints for table `permission_role`
--
ALTER TABLE `permission_role`
  ADD CONSTRAINT `permission_role_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`),
  ADD CONSTRAINT `permission_role_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `role_user`
--
ALTER TABLE `role_user`
  ADD CONSTRAINT `role_user_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `role_user_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
