<?php

namespace App\Modules\Admin\Controllers;

use App\Http\Controllers\Controller;
use App\Http\Requests;
use App\Modules\App\Models\Appointment;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class AdminController extends Controller
{
    /**
     * Display dashboard
     *
     * @return Response
     */
    public function index()
    {
        $today = Appointment::with('patient')->today()->paginate(5);
        $upcoming = Appointment::with('patient')->upcoming()->paginate(5);

        $todayCount = $today->firstItem();
        $upcomingCount = $upcoming->firstItem();

        return view('Admin::index', compact('today', 'upcoming', 'todayCount', 'upcomingCount'));
    }

    public function viewLogin()
    {
        return view("Admin::login.index");
    }

    public function login(Request $request)
    {
        if(Auth::attempt($request->only('email', 'password'), $request->only('remember_me')))
            return redirect()->intended(route('admin.index'))->withFlashMessage('Welcome Back!')->withFlashType('info');
        else
            if(Auth::viaRemember())
                return redirect()->intended(route('admin.index'))->withFlashMessage('Welcome Back!')->withFlashType('info');

        return redirect(route('admin.login.index'))->withFlashMessage('Login Failed!')->withFlashType('danger');
    }

    public function logout(Request $request)
    {
        \Auth::logout();
        \Session::flush();
        return redirect(route('admin.login.index'));
    }

    /* Fot TESTING */
    public function settings() {
        return view('Admin::settings');
    }
}
