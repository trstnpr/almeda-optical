<?php

namespace App\Modules\Admin\Models;

use App\Traits\ObfuscatorTrait;
use Zizaco\Entrust\EntrustPermission;

class Permission extends EntrustPermission
{
    use ObfuscatorTrait;
}
