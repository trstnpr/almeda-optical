<?php

namespace App\Modules\Admin\Models;

use App\Traits\ObfuscatorTrait;
use Zizaco\Entrust\EntrustRole;

class Role extends EntrustRole
{
    use ObfuscatorTrait;
}
