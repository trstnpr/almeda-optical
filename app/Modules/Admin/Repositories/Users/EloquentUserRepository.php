<?php

namespace App\Modules\Admin\Repositories\Users;

use App\Modules\Admin\Models\Users;

class EloquentUserRepository implements AppointmentRepository
{
    public function perPage()
    {
        return config('modules.perpage');
    }

    public function getModel()
    {
        return config('modules.models.user');
    }

    public function allOrSearch($searchQuery = null)
    {
        if (is_null($searchQuery)) {
            return $this->getAll();
        }

        return $this->search($searchQuery);
    }

    public function getAll()
    {
        return $this->getModel()->latest()->paginate($this->perPage());
    }

    public function search($searchQuery)
    {
        $search = "%{$searchQuery}%";

        return $this->getModel()->where('first_name', 'like', $search)
            ->orWhere('last_name', 'like', $search)
            ->paginate($this->perPage());
    }

    public function findById($id)
    {
        return $this->getModel()->find($id);
    }

    public function findBy($key, $value, $operator = '=')
    {
        return $this->getModel()->where($key, $operator, $value)->paginate($this->perPage());
    }

    public function delete($id)
    {
        $user = $this->findById($id);

        if (!is_null($user)) {
            $user->delete();

            return true;
        }

        return false;
    }

    public function create(array $data)
    {
        return $this->getModel()->create($data);
    }
}
