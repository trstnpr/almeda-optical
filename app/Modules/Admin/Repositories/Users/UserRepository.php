<?php

namespace App\Modules\Admin\Repositories\Users;

use App\Modules\Admin\Repositories\Repository;

interface UserRepository extends Repository { }
