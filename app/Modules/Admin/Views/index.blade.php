@extends('admin')

@section('stylesheet')
	<link href="{{ elixir('assets/admin/views/index.css') }}" rel="stylesheet">
@stop

@section('content')
    <div class="container-fluid">

    	<h2 class="content-header">Dashboard</h2>
    	<ul class="breadcrumb">
			<li><a href="javascript:void(0)">Admin</a></li>
			<li>Dashboard</li>
		</ul>

		<div class="row">
			<div class="col-md-6">
				<div class="well calendar" id="calendar-schedule"></div>
			</div>

			<div class="col-md-6">
				<div class="panel panel-warning appointment-today">
					<div class="panel-heading">Today's Appointments <span class="pull-right"><a href="{{ route('appointments.index') }}"><i class="fa fa-arrow-right"></i></a></span></div>
					<div class="panel-body table-responsive" style="padding:0px;">
						<table class="table table-striped table-hover ">
							<thead>
								<tr>
									<th>#</th>
									<th>Date & time</th>
									<th>Customer</th>
									<th>Contact</th>
									<th>Type</th>
									<th>Action</th>
								</tr>
							</thead>
							<tbody>
								@if(isset($todayCount))
									@foreach ($today as $appointment)
									<tr>
										<td>{!! $todayCount++ !!}</td>
										<td>{!! $appointment->due_at->format('F d, Y h:i A') !!}</td>
										<td>
                                            <a href="{!! route('patients.show', $appointment->patient->id_link) !!}">{!! $appointment->patient->first_name . ' ' . $appointment->patient->last_name !!}</a>
                                        </td>
										<td>{!! $appointment->patient->cell_number !!}</td>
										<td>{!! $appointment->type !!}</td>
										<td><button class="btn btn-info btn-raised btn-xs">Mark as Done</button></td>
									</tr>
									@endforeach
								@else
									<tr>
										<td class="text-center" colspan="6">
											No Appointments today
										</td>
									</tr>
								@endif
							</tbody>
						</table>

						<div class="text-center">
							{{ pagination_links($today) }}
						</div>
					</div>
				</div>
			</div>

			<div class="col-md-12">
				<div class="panel panel-info appointment-today">
					<div class="panel-heading">Upcoming Appointments <span class="pull-right"><a href="{{ route('appointments.index') }}"><i class="fa fa-arrow-right"></i></a></span></div>
					<div class="panel-body table-responsive" style="padding:0px;">
						<table class="table table-striped table-hover ">
							<thead>
								<tr>
									<th>#</th>
									<th>Date & Time</th>
									<th>Customer</th>
									<th>Contact</th>
									<th>Type</th>
									<th>Action</th>
								</tr>
							</thead>
							<tbody>
								@if(isset($upcomingCount))
									@foreach ($upcoming as $appointment)
									<tr>
										<td>{!! $upcomingCount++ !!}</td>
										<td>{!! $appointment->due_at->format('F d, Y h:i A') !!}</td>
										<td>
                                            <a href="{!! route('patients.show', $appointment->patient->id_link) !!}">{!! $appointment->patient->first_name . ' ' . $appointment->patient->last_name !!}</a>
                                        </td>
										<td>{!! $appointment->patient->cell_number !!}</td>
										<td>{!! $appointment->type !!}</td>
										<td><button class="btn btn-info btn-raised btn-xs">Mark as Done</button></td>
									</tr>
									@endforeach
								@else
									<tr>
										<td class="text-center" colspan="6">
											No Appointments upcoming
										</td>
									</tr>
								@endif
							</tbody>
						</table>

						<div class="text-center">
							{{ pagination_links($upcoming) }}
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="modal fade" id="schedule_view" tabindex="-1" role="dialog">
		<div class="modal-dialog modal-lg" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					<h4 class="modal-title"></h4>
				</div>
				<div class="modal-body table-responsive" style="padding:10px;"></div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				</div>
			</div>
		</div>
	</div>
@stop

@section('footer')
	<script src="{{ elixir('assets/jqueryui.js') }}"></script>
	<script src="{{ elixir('assets/admin/views/index.js') }}"></script>
@stop
