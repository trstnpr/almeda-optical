<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="initial-scale=1, width=device-width">
        <title>Almeda Optical</title>
        <link rel="apple-touch-icon" sizes="180x180" href="/apple-touch-icon.png">
        <link rel="icon" type="image/png" href="/favicon-32x32.png" sizes="32x32">
        <link rel="icon" type="image/png" href="/favicon-16x16.png" sizes="16x16">
        <link rel="manifest" href="/manifest.json">
        <link rel="mask-icon" href="/safari-pinned-tab.svg" color="#5bbad5">
        <meta name="theme-color" content="#ffffff">
        <link href="{{ elixir('assets/admin/admin.css') }}" rel="stylesheet">
        <link href="{{ elixir('assets/admin/views/login/index.css') }}" rel="stylesheet">
    </head>

    <body class="login-page" style="background-image:url('{{ asset('images/banner.jpg') }}');" >
        <div class="overlay">
            <div class="container">
                <div class="row">
                    <div class="col-md-4 col-md-offset-4">
                        <img class="img-responsive" src="{{ asset('images/logo_2.png') }}" style="margin: 0 auto;"/>
                        <br/>
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h2 class="panel-title">Administrator</h2>
                            </div>
                            <div class="panel-body">
                                @include('Admin::partials.flash')
                                <form accept-charset="UTF-8" role="form" class="admin-login" action="{{ url('admin/login') }}" method="post">
                                    {!! csrf_field() !!}
                                    <fieldset>
                                        <div class="form-group label-static">
                                            <label class="form-label">Username or Email</label>
                                            <input class="form-control" placeholder="Username or Email" name="email" type="text" id="email" required />
                                        </div>
                                        <div class="form-group label-static">
                                            <label class="form-label">Password</label>
                                            <input class="form-control" placeholder="Password" name="password" type="password" id="password" required />
                                        </div>
                                        <div class="checkbox">
                                            <label>
                                                <input name="remember_me" type="checkbox" value="1" /> Remember Me
                                            </label>
                                        </div>
                                        <br/>
                                        <div class="form-group">
                                            <button class="btn btn-block btn-raised btn-lg btn-login" type="submit">Sign in</button>
                                        </div>
                                    </fieldset>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <script src="{{ elixir('assets/admin/admin.js') }}"></script>
    </body>
</html>
