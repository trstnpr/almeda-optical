@if(Session::has('flash_message'))
<div class="alert flash-message alert-dismissible alert-{!! Session::get('flash_type', 'info') !!}">
  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
  {!! Session::get('flash_message') !!}
</div>
@endif
