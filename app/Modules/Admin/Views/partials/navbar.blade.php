<div class="navbar navbar-default navbar-fixed-top">
  <div class="container-fluid">
    <div class="navbar-header">
      <a class="navbar-brand" href="#menu-toggle" id="menu-toggle">
        <i class="fa fa-bars hidden-lg hidden-md"></i>
        <span class="hidden-xs">Almeda Optical</span>
      </a>
      {!! Form::open(['method' => 'GET', 'class' => 'hidden-lg hidden-md'])!!}
        <div class="input-group navbar-search">
            <input type="text" name="q" class="form-control" placeholder="Search..."/>
            <span class="input-group-btn">
              <button type='submit' name='search' id='search-btn' class="btn btn-flat"><i class="fa fa-search"></i></button>
            </span>
        </div>
      {!! Form::close() !!}
    </div>
    <div class="navbar-collapse collapse navbar-responsive-collapse hidden-sm hidden-xs">
      {!! Form::open(['method' => 'GET', 'class' => 'navbar-form navbar-right'])!!}
        <div class="input-group navbar-search">
            <input type="text" name="q" class="form-control" placeholder="Search..."/>
            <span class="input-group-btn">
              <button type='submit' name='search' id='search-btn' class="btn btn-flat"><i class="fa fa-search"></i></button>
            </span>
        </div>
      {!! Form::close() !!}
    </div>
  </div>
</div>