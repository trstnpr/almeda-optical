<div id="sidebar-wrapper">
    <ul class="sidebar-nav">
        <li>
            <div class="img-circle center-block">
                <img class="img-responsive" {{-- src="{!! Auth::user()->avatar !!}" --}} src="https://www.rit.edu/science/sites/rit.edu.science/files/root/man-placeholder_27.jpg" alt="User Image">
            </div>
        </li>
        @if (Auth::check())
            <li><a href="javascript:void(0)" data-target="#profile-dropdown" data-toggle="collapse"><i class="fa fa-user-secret"></i> <span>{!! Auth::user()->first_name !!} <small>({!! Auth::user()->email !!})</small> <i class="caret"></i></span></a></li>
            <div id="profile-dropdown" class="collapse">
                <li><a href="{{ url('admin/settings') }}"><i class="fa fa-wrench"></i> Settings</a></li>
                <li><a href="{{ route('admin.logout.index') }}"><i class="fa fa-sign-out"></i> Logout</a></li>
            </div>
        @else
            <li class=""><a href="">Login</a></li>
        @endif
        <li class="divider"></li>
        <li><a href="{{ route('admin.index') }}"><i class="fa fa-tachometer"></i> Dashboard</a></li>
        <li class="{{ isActiveRoute('admin/appointments') }}"><a href="{{ route('appointments.index') }}"><i class="fa fa-calendar"></i> Appointments</a></li>
        <li class="{{ isActiveRoute('admin/patients') }}"><a href="{{ route('patients.index') }}"><i class="fa fa-users"></i> Patients</a></li>
        <li class="{{ isActiveRoute('admin/logs') }}"><a href="{{ route('logs.index') }}"><i class="fa fa-history"></i> Logs</a></li>
        <li class="{{ isActiveRoute('admin/items') }}"><a href="{{ route('items.index') }}"><i class="fa fa-database"></i> Items</a></li>
        <li class="{{ isActiveRoute('admin/orders') }}"><a href="{{ route('orders.index') }}"><i class="fa fa-shopping-bag"></i> Orders</a></li>
    </ul>
</div>