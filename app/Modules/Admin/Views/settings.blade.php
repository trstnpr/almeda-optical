@extends('admin')

@section('stylesheet')
<link href="{{ elixir('assets/admin/views/index.css') }}" rel="stylesheet">
@stop

@section('content')

    <div class="container-fluid">

    	<h2 class="content-header">Settings</h2>
    	<ul class="breadcrumb">
			<li><a href="javascript:void(0)">Admin</a></li>
			<li>Settings</li>
		</ul>
	</div>

@stop

@section('footer')
	<script src="{{ elixir('assets/jqueryui.js') }}"></script>
	<script src="{{ elixir('assets/admin/views/index.js') }}"></script>
@stop
