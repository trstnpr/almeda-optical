<?php
/*
|--------------------------------------------------------------------------
| Detect Active Route
|--------------------------------------------------------------------------
|
| Compare given route with current route and return output if they match.
| Very useful for navigation, marking if the link is active.
|
*/
function isActiveRoute($route, $output = 'active')
{
    if (Request::is($route . '*')) return $output;
}

/*
|--------------------------------------------------------------------------
| Detect Active Routes
|--------------------------------------------------------------------------
|
| Compare given routes with current route and return output if they match.
| Very useful for navigation, marking if the link is active.
|
*/
function areActiveRoutes(Array $routes, $output = 'active')
{
    foreach ($routes as $route)
    {
        if (Request::is($route . '*')) return $output;
    }

}

function pagination_links($data, $view = null)
{
    if ($query = Request::query()) {
        $request = array_except($query, 'page');

        return $data->appends($request)->render($view);
    }

    return $data->render($view);
}
