<?php
// FakeId bindings
Route::obfuscate('users', 'App\Modules\Admin\Models\User');
Route::obfuscate('roles', 'App\Modules\Admin\Models\Role');
Route::obfuscate('permissions', 'App\Modules\Admin\Models\Permission');

Route::group(['module' => 'Admin', 'namespace' => 'App\Modules\Admin\Controllers', 'middleware' => ['web']], function() {
    Route::get('admin/login', ['as' => 'admin.login.index', 'uses' => 'AdminController@viewLogin', 'middleware' => 'guest']);
    Route::post('admin/login', ['as' => 'admin.login.post', 'uses' => 'AdminController@login']);

    // FOR TESTING VIEW
    Route::get('admin/settings', 'AdminController@settings');

    Route::group(['prefix' => 'admin', 'middleware' => ['auth']], function() {
        Route::get('/', ['as' => 'admin.index', 'uses' => 'AdminController@index']);
        Route::get('logout', ['as' => 'admin.logout.index', 'uses' => 'AdminController@logout']);

        Route::resource('users', 'UserController', [
            'names' => [
                'index'     => 'users.index',
                'create'    => 'users.create',
                'store'     => 'users.store',
                'show'      => 'users.show',
                'edit'      => 'users.edit',
                'update'    => 'users.update',
                'destroy'   => 'users.delete'
            ]
        ]);

        Route::resource('roles', 'RoleController', [
            'names' => [
                'index'     => 'roles.index',
                'create'    => 'roles.create',
                'store'     => 'roles.store',
                'show'      => 'roles.show',
                'edit'      => 'roles.edit',
                'update'    => 'roles.update',
                'destroy'   => 'roles.delete'
            ]
        ]);

        Route::resource('permissions', 'PermissionController', [
            'names' => [
                'index'     => 'permissions.index',
                'create'    => 'permissions.create',
                'store'     => 'permissions.store',
                'show'      => 'permissions.show',
                'edit'      => 'permissions.edit',
                'update'    => 'permissions.update',
                'destroy'   => 'permissions.delete'
            ]
        ]);
    });
});
