<?php

namespace App\Modules\App\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;

class AppController extends Controller {

	/**
	 * Display landing page.
	 *
	 * @return Response
	 */
	public function index()
	{
		return view("App::index");
	}
}
