<?php

namespace App\Modules\App\Controllers;

use App\Http\Controllers\Controller;
use App\Http\Requests;
use App\Modules\App\Models\Appointment;
use App\Modules\App\Models\Patient;
use App\Modules\App\Repositories\Appointments\AppointmentRepository;
use App\Modules\App\Validators\Appointments\Create;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;

class AppointmentController extends Controller
{
    /**
     * Instance of AppointmentController Interface
     * @var App\Modules\App\Repositories\Appointments\AppointmentRepository
     */
    protected $repository;

    /**
     * Class Contstructor
     * @param App\Modules\App\Repositories\Appointments\AppointmentRepository $repository interface implementation
     */
    public function __construct(AppointmentRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $appointments = $this->repository->allOrSearch($request->q);
        $count = $appointments->firstItem();

        if ($request->ajax())
            return json_encode([
                    'status' => 'success',
                    'message' => 'Record',
                    'data' => view('App::appointments.table', compact('appointments', 'count'))->render()
                ]);
        else
            return view('App::appointments.index', compact('appointments', 'count'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('App::appointments.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Create $request)
    {
        $data = $request->all();

        $data['due_at'] = Carbon::parse($data['due_date'] . ' ' . $data['due_time'])->format('Y-m-d H:i:s');

        Patient::create($request->except(['due_time', 'due_date', 'due_at']))
            ->appointment()->create($data);

        if ($request->ajax())
            return json_encode(['status' => 'success', 'message' => 'Successfully booked an appointment!']);
        else
            return redirect(route('appointments.index'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $appointment = $this->repository->findById($id);

        return view('App::appointments.show', compact('appointment'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        try {
            $appointment = $this->repository->findById($id);

            return view('App::appointments.edit');
        } catch (ModelNotFoundException $e) { }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try {
            $data = $request->all();
            $appointment = $this->repository->findById($id);

            $appointments->update($data);

            return redirect(route('appointments.index'));
        } catch (ModelNotFoundException $e) { }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $appointment = $this->repository->findById($id);

            $this->repository->delete($appointment);

            return redirect(route('admin.index'));
        } catch (ModelNotFoundException $e) { }
    }

    // TEST
    public function patientAppointment() {
        return view('App::appointments.patient');
    }
}
