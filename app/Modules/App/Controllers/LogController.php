<?php

namespace App\Modules\App\Controllers;

use App\Http\Controllers\Controller;
use App\Http\Requests;
use App\Modules\App\Repositories\Logs\LogRepository;
use Illuminate\Http\Request;

class LogController extends Controller
{
    /**
     * Instance of AppointmentController Interface
     * @var App\Modules\App\Repositories\Logs\LogRepository
     */
    protected $repository;

    /**
     * Class Contstructor
     * @param App\Modules\App\Repositories\Logs\LogRepository $repository interface implementation
     */
    public function __construct(LogRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $logs = $this->repository->allOrSearch($request->q);
        $count = $logs->firstItem();

        if ($request->ajax())
            return json_encode([
                    'status' => 'success',
                    'message' => 'Record',
                    'data' => view('App::logs.table', compact('logs', 'count'))->render()
                ]);
        else
            return view('App::logs.index', compact('logs', 'count'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
