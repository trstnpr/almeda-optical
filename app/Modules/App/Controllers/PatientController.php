<?php

namespace App\Modules\App\Controllers;

use App\Http\Controllers\Controller;
use App\Http\Requests;
use App\Modules\App\Repositories\Patients\PatientRepository;
use Illuminate\Http\Request;

class PatientController extends Controller
{
    /**
     * Instance of PatientRepository Interface
     * @var App\Modules\App\Repositories\Patients\PatientRepository
     */
    protected $repository;

    /**
     * Class Contstructor
     * @param App\Modules\App\Repositories\Patients\PatientRepository $repository interface implementation
     */
    public function __construct(PatientRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $patients = $this->repository->allOrSearch($request->q);
        $count = $patients->firstItem();

        if ($request->ajax())
            return json_encode([
                    'status' => 'success',
                    'message' => 'Record',
                    'data' => view('App::patients.table', compact('patients', 'count'))->render()
                ]);
        else
            return view('App::patients.index', compact('patients', 'count'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('App::patients.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->all();

        $this->repository->create($data);

        if ($request->ajax())
            return json_encode(['status' => 'success', 'message' => 'Successfully added new patient']);
        else
            return redirect(route('patients.index'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $patient = $this->repository->findById($id);

        return view('App::patients.show', compact('patient'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try {
            $data = $request->all();
            $patient = $this->repository->findById($id);

            $patient->update($data);

            return redirect(route('patients.show', $id))->withFlashMessage('Updated')->withFlashType('success');
        } catch (ModelNotFoundException $e) { }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function patientUser() {
        return view('App::patients.patient');
    }
}
