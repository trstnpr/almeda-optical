<?php

namespace App\Modules\App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use App\Traits\ObfuscatorTrait;

class Appointment extends Model
{
    use ObfuscatorTrait;

    /**
     * Mass assignable fields
     * @var array
     */
    protected $fillable = ['type', 'notes', 'due_at'];

    /**
     * Carbon instances(dates)
     * @var array
     */
    protected $dates =['due_at'];

    public function scopeToday($query)
    {
        $query->whereBetween('due_at', [Carbon::today(), Carbon::today()->addDay()]);
    }

    public function scopeUpcoming($query)
    {
        $query->where('due_at', '>', Carbon::today()->addDay());
    }
    /**
     * Get the patient attached to this appointment.
     * @return Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function patient()
    {
        return $this->belongsTo(config('modules.models.patient'));
    }

    /**
     * Get the log attached to this appointment.
     * @return Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function log()
    {
        return $this->hasOne(config('modules.models.log'));
    }
}
