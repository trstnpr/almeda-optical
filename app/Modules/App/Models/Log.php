<?php

namespace App\Modules\App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Traits\ObfuscatorTrait;

class Log extends Model
{
    use ObfuscatorTrait;

    /**
     * Mass assignable fields
     * @var array
     */
    protected $fillable = ['left_spherical', 'left_cylindrical', 'left_axis', 'left_prism', 'left_base', 'left_add', 'right_spherical', 'right_cylindrical', 'right_axis', 'right_prism', 'right_base', 'right_add', 'both_ou', 'notes'];

    /**
     * Get the user that created this log.
     * @return Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function appointment()
    {
        return $this->belongsTo(config('modules.models.appointment'));
    }

    /**
     * Get the patient attached to this log.
     * @return Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function patient()
    {
        return $this->belongsTo(config('modules.models.patient'));
    }

    /**
     * Get the user that created this log.
     * @return Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(config('modules.models.user'));
    }
}
