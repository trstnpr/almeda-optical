<?php

namespace App\Modules\App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Traits\ObfuscatorTrait;

class Patient extends Model
{
    use ObfuscatorTrait;

    /**
     * Mass assignable fields
     * @var array
     */
    protected $fillable = [
        'last_name',
        'first_name',
        'middle_name',
        'address_line_1',
        'address_line_2',
        'address_city',
        'address_state',
        'address_zipcode',
        'cell_number',
        'email'
    ];

    public function getFullNameAttribute()
    {
        return $this->last_name . ', ' . $this->first_name . ' ' . $this->middle_name;
    }

    public function getFullAddressAttribute()
    {
        return $this->address_line_1 . ', ' . $this->address_line_2 . ', ' . $this->address_city . ', ' . $this->address_state . ' ' . $this->address_zipcode;
    }

    /**
     * Get the appointment attached to this user.
     * @return Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function appointment()
    {
        return $this->hasMany(config('modules.models.appointment'));
    }

    /**
     * Get the log attached to this user.
     * @return Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function log()
    {
        return $this->hasMany(config('modules.models.log'));
    }
}
