<?php

namespace App\Modules\App\Providers;

use Illuminate\Support\ServiceProvider;

class AppRepositoriesServiceProvider extends ServiceProvider
{
    /**
     * Indicates if loading of the provider is deferred.
     *
     * @var bool
     */
    protected $defer = false;

    protected $entities = [
        'Appointment',
        'Log',
        'Patient'
    ];

    /**
     * Register the service provider.
     */
    public function register()
    {
        foreach ($this->entities as $entity) {
            $this->{'bind'.$entity.'Repository'}();
        }
    }

    protected function bindAppointmentRepository()
    {
        $this->app->bind(
            'App\Modules\App\Repositories\Appointments\AppointmentRepository',
            'App\Modules\App\Repositories\Appointments\EloquentAppointmentRepository'
        );
    }

    protected function bindLogRepository()
    {
        $this->app->bind(
            'App\Modules\App\Repositories\Logs\LogRepository',
            'App\Modules\App\Repositories\Logs\EloquentLogRepository'
        );
    }

    protected function bindPatientRepository()
    {
        $this->app->bind(
            'App\Modules\App\Repositories\Patients\PatientRepository',
            'App\Modules\App\Repositories\Patients\EloquentPatientRepository'
        );
    }
}
