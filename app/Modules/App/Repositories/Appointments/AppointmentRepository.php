<?php

namespace App\Modules\App\Repositories\Appointments;

use App\Modules\App\Repositories\Repository;

interface AppointmentRepository extends Repository { }
