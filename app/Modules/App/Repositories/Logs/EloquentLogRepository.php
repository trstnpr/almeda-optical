<?php

namespace App\Modules\App\Repositories\Logs;

use App\Modules\App\Models\Logs;
use Carbon\Carbon;

class EloquentLogRepository implements LogRepository
{
    public function perPage()
    {
        return config('modules.perpage');
    }

    public function getModel()
    {
        $model = config('modules.models.log');

        return new $model;
    }

    public function allOrSearch($searchQuery = null)
    {
        if (is_null($searchQuery)) {
            return $this->getAll();
        }

        return $this->search($searchQuery);
    }

    public function getAll()
    {
        return $this->getModel()->latest()->paginate($this->perPage());
    }

    public function search($searchQuery)
    {
        $search = "%{$searchQuery}%";

        if(check_if_valid_date($searchQuery)) {
            return $this->getModel()->with('appointment', 'patient', 'user')
                ->whereHas('patient', function($query) use($search) {
                    $query->whereBetween(
                        'due_at', [
                            Carbon::parse($searchQuery)->format('Y-m-d H:i:s'),
                            Carbon::parse($searchQuery)->addDay()->format('Y-m-d H:i:s')
                        ]
                    );
                })->paginate($this->perPage());
        } else {
            return $this->getModel()->with('appointment', 'patient', 'user')
                ->whereHas('patient', function($query) use($search) {
                    $query->where('last_name', 'like', $search)
                    ->orWhere('first_name', 'like', $search)
                    ->orWhere('middle_name', 'like', $search);
                })->orWhereHas('user', function($query) use($search) {
                    $query->where('last_name', 'like', $search)
                    ->orWhere('first_name', 'like', $search)
                    ->orWhere('middle_name', 'like', $search);
                })->paginate($this->perPage());
        }
    }

    public function findById($id)
    {
        return $this->getModel()->find($id);
    }

    public function findBy($key, $value, $operator = '=')
    {
        return $this->getModel()->where($key, $operator, $value)->paginate($this->perPage());
    }

    public function delete($id)
    {
        $article = $this->findById($id);

        if (!is_null($article)) {
            $article->delete();

            return true;
        }

        return false;
    }

    public function create(array $data)
    {
        return $this->getModel()->create($data);
    }
}
