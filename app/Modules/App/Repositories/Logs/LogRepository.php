<?php

namespace App\Modules\App\Repositories\Logs;

use App\Modules\App\Repositories\Repository;

interface LogRepository extends Repository { }
