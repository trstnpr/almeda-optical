<?php

namespace App\Modules\App\Repositories\Patients;

use App\Modules\App\Models\Patient;
use Carbon\Carbon;

class EloquentPatientRepository implements PatientRepository
{
    public function perPage()
    {
        return config('modules.perpage');
    }

    public function getModel()
    {
        $model = config('modules.models.patient');

        return new $model;
    }

    public function allOrSearch($searchQuery = null)
    {
        if (is_null($searchQuery)) {
            return $this->getAll();
        }

        return $this->search($searchQuery);
    }

    public function getAll()
    {
        return $this->getModel()->latest()->paginate($this->perPage());
    }

    public function search($searchQuery)
    {
        $search = "%{$searchQuery}%";

        return $this->getModel()->with('logs')
            ->whereHas('logs', function($query) {
                $query->latest();
            })->paginate($this->perPage());

    }

    public function findById($id)
    {
        return $this->getModel()->find($id);
    }

    public function findBy($key, $value, $operator = '=')
    {
        return $this->getModel()->where($key, $operator, $value)->paginate($this->perPage());
    }

    public function delete($id)
    {
        $patient = $this->findById($id);

        if (!is_null($patient)) {
            $patient->delete();

            return true;
        }

        return false;
    }

    public function create(array $data)
    {
        return $this->getModel()->create($data);
    }
}
