<?php

namespace App\Modules\App\Repositories\Patients;

use App\Modules\App\Repositories\Repository;

interface PatientRepository extends Repository { }
