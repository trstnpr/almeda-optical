<?php

namespace App\Modules\App\Validators\Appointments;

use App\Http\Requests\Request;
use Carbon\Carbon;

class Create extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'type' => 'required',
            'due_date' => 'required|date|after:' . Carbon::today()->format('m/d/Y'),
            'last_name' => 'required',
            'first_name' => 'required',
            'middle_name' => 'required',
            'address_line_1' => 'required',
            'address_line_2' => 'required',
            'address_city' => 'required',
            'address_state' => 'required',
            'address_zipcode' => 'required',
            'cell_number' => 'required',
            'email' => 'required',
            'g-recaptcha-response' => 'required',
        ];
    }

    /**
     * Make custom error messages.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'due_date.after' => 'Appointment must be in the future!',
            'g-recaptcha-response.required' => 'Please check the captcha to continue.',
        ];
    }
}
