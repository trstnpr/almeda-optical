@extends('admin')

@section('stylesheet')
<link href="{{ elixir('assets/admin/views/index.css') }}" rel="stylesheet">
@stop

@section('content')

	<div class="container-fluid">

		<h2 class="content-header">
			Appointments
		</h2>

		<ul class="breadcrumb">
			<li><a href="javascript:void(0)">Dashboard</a></li>
			<li>Appointments</li>
		</ul>

		<div class="row">
			<div class="col-md-12">
				<div class="panel panel-warning appointment-today">
					<div class="panel-heading">Today's Appointments <span class="pull-right">{!! \Carbon\Carbon::now()->format('F j, Y') !!} </span></div>
					<div class="panel-body table-responsive" style="padding:0px;">
						<table class="table table-striped table-hover ">
							<thead>
								<tr>
									<th>#</th>
									<th>Date & Time</th>
									<th>Customer</th>
									<th>Contact</th>
									<th>Type</th>
									<th>Actions</th>
								</tr>
							</thead>
							<tbody>
								@if(isset($count))
									@foreach($appointments as $appointment)
									<tr>
										<td>{!! $count++ !!}</td>
										<td>{!! $appointment->due_at->format('F d, Y h:i A') !!}</td>
										<td>
											<a href="{!! route('patients.show', $appointment->patient->id_link) !!}">{!! $appointment->patient->first_name . ' ' . $appointment->patient->last_name !!}</a>
										</td>
										<td>{!! $appointment->patient->cell_number !!}</td>
										<td>{!! $appointment->type !!}</td>
										<td><a href="{!! route('appointments.show', $appointment->id_link) !!}" class="btn btn-info btn-raised btn-xs">View Details</a></td>
									</tr>
									@endforeach
								@else
									<tr>
										<td class="text-center" colspan="6">
											No Appointments
										</td>
									</tr>
								@endif
							</tbody>
						</table>
						<div class="text-center">
							{{ pagination_links($appointments) }}
						</div>
					</div>
				</div>
			</div>
			<div class="col-md-12">
				<div class="panel panel-primary form-appointment" style="display:none;">
					<div class="panel-heading">Add Appointment</div>
					<div class="panel-body">
						<form class="appointment-form">
							<div class="row">
								<div class="form-group label-static col-md-4">
									<label class="form-label">Date</label>
									<input type="text" class="form-control" placeholder="Appoinment date" name="due_date" id="due_date"/>
								</div>
								<div class="form-group label-static col-md-4">
									<label class="form-label">Time</label>
									<select class="form-control" name="due_time" id="due_time">
										<option selected disabled>-- Start time --</option>
										<option value="08:00 AM">08:00 AM</option>
										<option value="09:00 AM">09:00 AM</option>
										<option value="10:00 AM">10:00 AM</option>
										<option value="11:00 AM">11:00 AM</option>
										<option value="12:00 PM">12:00 PM</option>
									</select>
								</div>
								<div class="form-group label-static col-md-4">
									<label class="form-label">Type</label>
									<select class="form-control" name="type" id="type">
										<option selected disabled>-- Start Type --</option>
										<option value="online">Online</option>
										<option value="walk-in">Walk In</option>
										<option value="on-call">Call</option>
									</select>
								</div>

								<div class="form-group label-static col-md-4">
									<label class="form-label">Last Name</label>
									<input type="text" class="form-control" placeholder="Your last Name" name="last_name" id="last_name"/>
								</div>
								<div class="form-group label-static col-md-4">
									<label class="form-label">First Name</label>
									<input type="text" class="form-control" placeholder="Your first name" name="first_name" id="first_name"/>
								</div>
								<div class="form-group label-static col-md-4">
									<label class="form-label">Middle Name</label>
									<input type="text" class="form-control" placeholder="Your middle Name" name="middle_name" id="middle_name"/>
								</div>

								<div class="form-group label-static col-md-8">
									<label class="form-label">Address</label>
									<input type="text" class="form-control" placeholder="House # & Street" name="address_line_1" id="address_line_1"/>
								</div>
								<div class="form-group label-static col-md-4">
									<label class="form-label">Barangay</label>
									<input type="text" class="form-control" placeholder="Barangay/Subdivision" name="address_line_2" id="address_line_2"/>
								</div>

								<div class="form-group label-static col-md-5">
									<label class="form-label">City</label>
									<input type="text" class="form-control" placeholder="City" name="address_city" id="address_city"/>
								</div>
								<div class="form-group label-static col-md-5">
									<label class="form-label">State</label>
									<input type="text" class="form-control" placeholder="State/Province" name="address_state" id="address_state"/>
								</div>
								<div class="form-group label-static col-md-2">
									<label class="form-label">ZIP</label>
									<input type="text" class="form-control" placeholder="Zip code" name="address_zipcode" id="address_zipcode"/>
								</div>

								<div class="form-group label-static col-md-6">
									<label class="form-label">Contact</label>
									<input type="text" class="form-control" placeholder="Your contact Number" name="cell_number" id="cell_number"/>
								</div>
								<div class="form-group label-static col-md-6">
									<label class="form-label">Email</label>
									<input type="email" class="form-control" placeholder="Your email address" name="email" id="email"/>
								</div>

								<div class="form-group label-static col-md-12">
									<label class="form-label">Appointment Notes</label>
									<textarea class="form-control" placeholder="Reason of appointment or inquiry" name="notes" id="notes"></textarea>
								</div>

								<div class="form-group label-static col-md-12">
									<button type="button" class="btn btn-raised btn-primary sched-cancel">Cancel <i class="fa fa-ban"></i></button>
									<button type="submit" class="btn btn-raised btn-primary sched-submit">Submit <i class="fa fa-send"></i></button>
								</div>

							</div>
						</form>
					</div>
				</div>
			</div>
		</div>

		<div class="fixed-action-btn" style="bottom: 45px; right: 24px;">
			<button class="btn btn-primary btn-raised btn-fab" data-toggle="modal" data-target="#calendar"><i class="material-icons">add</i></button>
		</div>

		<!-- Modal Calendar -->
		<div class="modal fade" id="calendar" data-backdrop="static" data-keyboard="false" tabindex="-1" role="dialog">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
						<h4 class="modal-title">Choose Date</h4>
					</div>

					<div class="modal-body calendar"></div>

					<div class="modal-footer">
						<button type="button" class="btn btn-default close-modal" data-dismiss="modal">Close</button>
						<button type="submit" class="btn btn-default form-submit hide">Submit</button>
					</div>
				</div>
			</div>
		</div>

	</div>
@stop

@section('footer')
	<script src="{{ elixir('assets/jqueryui.js') }}"></script>
	<script src="{{ elixir('assets/admin/views/appointment/index.js') }}"></script>
@stop