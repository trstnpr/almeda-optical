@extends('admin')

@section('stylesheet')
<link href="{{ elixir('assets/admin/views/index.css') }}" rel="stylesheet">
@stop

@section('content')

    <div class="container-fluid">

    	<h2 class="content-header">
    		Appointment Details
		</h2>

    	<ul class="breadcrumb">
			<li><a href="javascript:void(0)">Dashboard</a></li>
			<li><a href="{!! route('appointments.index') !!}">Appointment</a></li>
			<li>Details</li>
		</ul>

		<div class="panel panel-warning">
			<div class="panel-heading">
				<h3 class="panel-title">Appointment</h3>
			</div>
			<div class="panel-body">
				<div class="form-group row">
					<div class="col-sm-2 ">
						<p><strong>Date & Time</strong></p>
					</div>
					<div class="col-sm-10">
						<p>{!! $appointment->due_at->format('F d, Y h:i A') !!}</p>
					</div>
				</div>
				<div class="form-group row">
					<div class="col-sm-2 ">
						<p><strong>Type</strong></p>
					</div>
					<div class="col-sm-10">
						<p class="badge badge-success">{!! $appointment->type !!}</p>
					</div>
				</div>
				<div class="form-group row">
					<div class="col-sm-2 ">
						<p><strong>Name</strong></p>
					</div>
					<div class="col-sm-10">
						<p>{!! $appointment->patient->full_name !!}</p>
					</div>
				</div>
				<div class="form-group row">
					<div class="col-sm-2 ">
						<p><strong>Address</strong></p>
					</div>
					<div class="col-sm-10">
						<p>{!! $appointment->patient->full_address !!}</p>
					</div>
				</div>
				<div class="form-group row">
					<div class="col-sm-2 ">
						<p><strong>Cell Number</strong></p>
					</div>
					<div class="col-sm-10">
						<p>{!! $appointment->patient->cell_number !!}</p>
					</div>
				</div>
				<div class="form-group row">
					<div class="col-sm-2 ">
						<p><strong>Email Address</strong></p>
					</div>
					<div class="col-sm-10">
						<p>{!! $appointment->patient->email !!}</p>
					</div>
				</div>
				<div class="form-group row">
					<div class="col-sm-2 ">
						<p><strong>Appointment Notes</strong></p>
					</div>
					<div class="col-sm-10">
						<p>{!! $appointment->notes !!}</p>
					</div>
				</div>
			</div>
		</div>

		<div class="fixed-action-btn" style="bottom: 45px; right: 24px;">
			<button class="btn btn-primary btn-raised btn-fab" data-toggle="modal" data-target="#doctor-prescription"><i class="material-icons">event_note</i></button>
		</div>

		<div class="modal fade" id="doctor-prescription" data-backdrop="static" data-keyboard="false" tabindex="-1" role="dialog">
			<div class="modal-dialog modal-lg">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
						<h4 class="modal-title">Doctor's Prescription</h4>
					</div>
					@include('App::logs.form-modal')
				</div>
			</div>
		</div>
	</div>
@stop

@section('footer')
	<script src="{{ elixir('assets/admin/views/index.js') }}"></script>
	<script src="{{ elixir('assets/jqueryui.js') }}"></script>
	<script type="text/javascript">
		$(document).ready(function() {
			
			$('#merchandise').on('shown.bs.collapse', function() {
				$('.btn-merch').html('<i class="fa fa-times"></i> Remove Merchandise');
			});
			$('#merchandise').on('hidden.bs.collapse', function() {
				$('.btn-merch').html('<i class="fa fa-plus"></i> Add Merchandise');
			});

			$('.cbx').on('change', function() {
				if(this.id == 'lens') {
					if(this.checked) {
						$('.item-list').append('<tr class="lens-row"><td>Lens</td><td><input type="text" class="form-control"/></td><td><input type="number" class="form-control item-price" id="lens-price"/></td></tr>');
					} else {
						$('.lens-row').remove();
					}
				} else if(this.id == 'contact') {
					if(this.checked) {
						$('.item-list').append('<tr class="contact-row"><td>Contact</td><td><input type="text" class="form-control"/></td><td><input type="number" class="form-control item-price" id="contact-price" value="0"/></td></tr>');
					} else {
						$('.contact-row').remove();
					}
				} else if(this.id == 'frame') {
					if(this.checked) {
						$('.item-list').append('<tr class="frame-row"><td>Frame</td><td><input type="text" class="form-control"/></td><td><input type="number" class="form-control item-price" id="frame-price" value="0"/></td></tr>');
					} else {
						$('.frame-row').remove();
					}
				}
			});

			$('#lens-price').keyup(function(){
		        $('.total-price').html($('#lens-price').val());
		    });

		});
	</script>
@stop