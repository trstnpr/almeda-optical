<table class="table table-striped table-hover">
    <thead>
        <tr>
            <th>Date & Time</th>
            <th>Customer</th>
            <th>Contact</th>
            <th>Location</th>
            <th>Type</th>
            <th>Action</th>
        </tr>
    </thead>
    <tbody class="appointment-data">
        @if(isset($count))
            @foreach($appointments as $appointment)
            <tr>
                <td>{{ $count++ }}</td>
                <td>{!! $appointment->due_at->format('F d, Y h:i A') !!}</td>
                <td>
                    <a href="{!! route('patients.show', $appointment->patient->id_link) !!}">{!! $appointment->patient->first_name . ' ' . $appointment->patient->last_name !!}</a></td>
                <td>{!! $appointment->patient->cell_number !!}</td>
                <td>{!! $appointment->type !!}</td>
                <td><button class="btn btn-info btn-raised btn-xs">Mark as Done</button></td>
            </tr>
            @endforeach
        @else
            <tr>
                <td class="text-center" colspan="6">
                    No Appointments
                </td>
            </tr>
        @endif
    </tbody>
</table>

<div class="text-center">
    {{ pagination_links($appointments) }}
</div>