	<section class="section-6 footer">
		<div class="container section-6-content">
			<div class="footer-menu">
				<div class="row">
					<div class="col-md-2 col-md-offset-1">
						<span class="menu">
							<a class="menu-scroll" href="#home">HOME</a>
						</span>
					</div>
					<div class="col-md-2">
						<span class="menu">
							<a class="menu-scroll" href="#about">ABOUT</a>
						</span>
					</div>

					<div class="col-md-2">
						<span class="menu">
							<a class="menu-scroll" href="#glass">Glasses</a>
						</span>
					</div>
					<div class="col-md-2">
						<span class="menu">
							<a class="menu-scroll" href="#eye_exams">EYE EXAMS</a>
						</span>
					</div>
					<div class="col-md-2 col-md-offset-1">
						<span class="menu">
							<a class="menu-scroll" href="#contact_lens">CONTACT LENSES</a>
						</span>
					</div>
				</div>
			</div>

			<hr style="border-top:2px solid #505050;" />

			<div class="footer-info">
				<div class="row">
					<div class="col-md-9">
						<ul class="list-unstyled">
							<li>(02) 752 7153</li>
							<li>2F Glorietta, Ayala Avenue Center, Makati</li>
							<li>info@almedaoptical.net</li>
						</ul>
					</div>
					<div class="col-md-3">
						<div class="social-btn pull-right">
							<a href="#"><i class="fa fa-twitter fa-5x"></i></a>
							&nbsp;&nbsp;&nbsp;&nbsp;
							<a href="#"><i class="fa fa-facebook-official fa-5x"></i></a>
						</div>
					</div>
				</div>
				<br/>
				Copyright &copy; 2016 Almeda Optical
			</div>
		</div>
	</section>