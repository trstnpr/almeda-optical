	<nav class="navbar navbar-fixed-top" role="navigation">
		<div class="container">
			<div class="navbar-header">
				<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-responsive-collapse">
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>
				<a class="navbar-brand menu-scroll" href="#home">Almeda Optical</a>
			</div>

			<div class="navbar-collapse collapse navbar-responsive-collapse">
				<ul class="nav navbar-nav navbar-right hidden-lg hidden-md">
					<li><a href="javascript:void(0)">HOME</a></li>
					<li><a href="javascript:void(0)">ABOUT</a></li>
					<li><a href="javascript:void(0)">EYE EXAMS</a></li>
					<li><a href="javascript:void(0)">CONTACT LENSES</a></li>
					<li><a href="javascript:void(0)">CONTACT</a></li>
				</ul>

				<ul class="nav navbar-nav navbar-right hidden-sm hidden-xs">
					<li class="dropdown">
						<a href="#" data-target="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-bars" aria-hidden="true"></i></a>
						<ul class="dropdown-menu" style="background-color:#ebcb80;">
							<li><a class="menu-scroll" href="#home">HOME</a></li>
							<li><a class="menu-scroll" href="#about">ABOUT</a></li>
							<li><a class="menu-scroll" href="#glass">GLASSES</a></li>
							<li><a class="menu-scroll" href="#eye_exams">EYE EXAMS</a></li>
							<li><a class="menu-scroll" href="#contact_lens">CONTACT LENSES</a></li>
						</ul>
					</li>
				</ul>
			</div>
		</div>
	</nav>
