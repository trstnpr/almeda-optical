	<section class="banner section-1" id="home" style="background-image:url('{{ asset('images/banner.jpg') }}');">
		<div class="banner-overlay">
			<div class="container section-1-content">
				<center>
					<h1 class="section-title" id="startLogo">Almeda Optical</h1>
					<div class="col-lg-10 col-lg-offset-1 hidden-xs">
						<p class="text">We are the leading provider of quality eyewear products in the Philippines such as eyeglasses, contact lenses, and sunglasses. A comprehensive eye checkup performed by our trained and competent optometrists will help you determine the right eyewear for you. We use only state-of-the-art facilities and eye care equipment. We are also the primary distributor of international eyewear brands like Adidas, Arrow, Converse, Penguin and more.</p>
					</div>
					<br/>

					<div class="row hidden-sm hidden-xs col-sm-12">

						<div class="col-md-3"></div>
						<div class="col-md-2">
							<img src="{{ asset('images/glasses.png') }}" class="img-responsive" />
						</div>
						<div class="col-md-2">
							<img src="{{ asset('images/exams.png') }}" class="img-responsive" />
						</div>
						<div class="col-md-2">
							<img src="{{ asset('images/lens.png') }}" class="img-responsive" />
						</div>
						<div class="col-md-3"></div>
					</div>
					<div class="col-lg-6 col-lg-offset-3">
						<button class="btn btn-raised" id="banner-calendar" data-toggle="modal" data-target="#calendar">Make an Appointment</button>
					</div>
				</center>
			</div>
		</div>
	</section>
