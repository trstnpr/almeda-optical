	<section class="section-2 startchange" id="about" style="background-image:url('{{ asset('images/gold.jpg') }}');">
		<div class="container section-2-content">
			<center>
				<h1 class="section-title">Almeda Optical</h1>
				<div class="underline-fff"></div>
				<div class="col-lg-10 col-lg-offset-1">
					<p class="text">
						Welcome to our practice. We are pleased to have the opportunity to meet all your vision and eye health care needs. We are a caring team of eye care professionals dedicated to providing the best eye health care, from infants to seniors. State of the art technology allows us to provide you with unsurpassed quality of care right here in the office. We emphasize preventive, therapeutic and developmental care to allow our patients to achieve their full potential.
					</p>
				</div>
				<div class="col-lg-6 col-lg-offset-3">
					<ul class="list-unstyled">
						<li><button class="btn btn-raised btn-default" id="btn-exam">EYE EXAMS</button></li>
						<li><button class="btn btn-raised btn-default" id="btn-lense">CONTACT LENSES</button></li>
					</ul>
				</div>
			</center>
		</div>
	</section>


