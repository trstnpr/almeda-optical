	<section class="section-3" id="glass">
		<section class="container section-3-content">
			<center>
				<h1 class="section-title">Glasses</h1>
				<div class="underline-000"></div>
				<p class="text">We offer an extensive inventory of designer eyewear for men, women and children. Many of our exclusive brands that are independently designed are not available to online vendors. We can provide alternative lens value packages that are competitive to brick and mortar retailers and popular online vendors. Our professionally trained opticians can recommend different collections that will defy conventional wisdom and provide the ultimate artistic look for your needs. We delight in the evolution of fashion trends and will help create your unique selection.</p>
				<div class="row">
					<div class="col-md-4">
						<div class="glasses-container">
							<img src="{{ asset('images/glasses/rayban-eyeglasses.jpg') }}" class="img-responsive" />
							<br/>
							<img src="{{ asset('images/glasses/rayban-logo.png') }}" class="img-responsive" width="25%" />
							<br/>
						</div>
					</div>
					<div class="col-md-4">
						<div class="glasses-container">
							<img src="{{ asset('images/glasses/michaelkors-eyeglasses.jpg') }}" class="img-responsive" />
							<br/>
							<img src="{{ asset('images/glasses/michaelkors-logo.png') }}" class="img-responsive" width="25%" />
							<br/>
						</div>
					</div>
					<div class="col-md-4">
						<div class="glasses-container">
							<img src="{{ asset('images/glasses/ralphlauren-eyeglasses.jpg') }}" class="img-responsive" />
							<br/>
							<img src="{{ asset('images/glasses/ralphlauren-logo.png') }}" class="img-responsive" width="40%" />
							<br/>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-md-4">
						<div class="glasses-container">
							<img src="{{ asset('images/glasses/arnette-eyeglasses.jpg') }}" class="img-responsive"/>
							<br/>
							<img src="{{ asset('images/glasses/arnette-logo.png') }}" class="img-responsive" width="25%" />
							<br/>
						</div>
					</div>
					<div class="col-md-4">
						<div class="glasses-container">
							<img src="{{ asset('images/glasses/oakley-eyeglasses.jpg') }}" class="img-responsive" />
							<br/>
							<img src="{{ asset('images/glasses/oakley-logo.png') }}" class="img-responsive" width="25%" />
							<br/>
						</div>
					</div>

					<div class="col-md-4">
						<div class="glasses-container">
							<img src="{{ asset('images/glasses/burberry-eyeglasses.jpg') }}" class="img-responsive"/>
							<br/>
							<img src="{{ asset('images/glasses/burberry-logo.png') }}" class="img-responsive" width="50%" />
							<br/>
						</div>
					</div>
				</div>
			</center>

		</section>
	</section>

	{{-- <div class="modal fade products" id="modal">
		<div class="modal-dialog modal-lg">
			<div class="modal-body product-container">
				<!--i class="fa fa-times fa-lg pull-right" data-dismiss="modal" aria-label="Close" style="margin-top:-12px;"></i-->
				<div class="row">
					<div class="col-md-8 product-preview">
						<div id="carousel-example-generic" class="carousel slide" data-ride="carousel" style>
							<div class="carousel-inner">
								<div class="item active">
									<img class="img-responsive" src="{{ asset('images/placeholder/1.jpg') }}" alt="Item 1">
								</div>
								<div class="item">
									<img class="img-responsive" src="{{ asset('images/placeholder/2.jpg') }}" alt="Item 2">
								</div>
								<div class="item">
									<img class="img-responsive" src="{{ asset('images/placeholder/3.jpg') }}" alt="Item 3">
								</div>
							</div>

							<!-- Controls -->
							<a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
								<span class="glyphicon glyphicon-chevron-left"></span>
							</a>
							<a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
								<span class="glyphicon glyphicon-chevron-right"></span>
							</a>
						</div>
					</div>
					<div class="col-md-4 product-info">
						<div class="first">
							<h4 class="product-title">Ray Ban XYZ</h4>
							<h3 class="product-price">PhP 5,000.00</h3>
							<p class="product-description">
								Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam molestie tortor augue, at scelerisque tortor pulvinar maximus. In dictum nec diam non feugiat.
							</p>
						</div>
						<div class="second">
							<button type="button" class="btn btn-raised btn-primary btn-block"><i class="fa fa-cart-plus"></i> Add to cart</button>
						</div>
						<div class="third">
							<p class="product-features"><strong>Features.</strong> Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam molestie tortor augue, at scelerisque tortor pulvinar maximus.</p>
							<p class="product-specification"><strong>Specifications.</strong> Full - 135mm, Frame - 51mm x 33mm, Nose - 18mm, Leg - 135mm, Material - Plastic</p>
						</div>
					</div>
				</div>
			</div>
			
			<div class="modal-body products-container">
				<h4>
					More items from <img class="img-responsive" src="{{ asset('images/glasses/rayban-logo.png') }}" style="display:inline-block;height:25px;"/>
				</h4>
				<div class="row">
					@for($i=0; $i<6; $i++)
						<div class="col-md-2 col-xs-6">
							<div class="other-item" style="background-color:#fff;border:1px solid #e7e7e7;margin-bottom:10px;">
								<img class="img-responsive" src="{{ asset('images/placeholder/1.jpg') }}" style="height:100px;margin:0 auto;"/>
							</div>
						</div>
					@endfor
				</div>
			</div>
		</div>
	</div>
 --}}