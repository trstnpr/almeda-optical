	<section class="section-4" id="eye_exams">
		<div class="container section-4-content">
			<div class="row">
				<div class="col-md-8">
					<h1 class="section-title">Eye Exams</h1>
					<div class="underline-gold"></div>
					<p class="text">
						Eye care starts with annual routine examinations. Your complete examination will reveal any eye problems, even hidden ones you may not be aware of. We are committed to providing you with the most advanced technology to care for your eye health as well as your visual comfort so the quality and efficiency of your vision helps you achieve your full potential at work, in the classroom, for hobbies, sports and everyday life.
					</p>
					<p class="text">
						If you have diabetes, high blood pressure, heart disease, arthritis, elevated cholesterol, or thyroid disease, your vision and the health of your eyes could be affected. Also, certain medications can cause visual problems, which is why it's so important to have your eyes examined on an annual basis.
					</p>
					<p class="text">
						We diagnose and treat many eye problems, including eye injuries, dry eyes, glaucoma, diabetic eye diseases, eye infections, macular degeneration and other retinal problems.
					</p>
				</div>
				<div class="col-md-4">
					<div class="services-container">
						<h3 class="section-title">Our Exam Services</h3>
						<div class="services">
							<ul class="list-unstyled">
								<li>LASIK consultations</li>
								<li>Cataract screenings</li>
								<li>Glaucoma screening</li>
								<li>Eye infections</li>
								<li>Post-operative LASIK care</li>
								<li>And more</li>
							</ul>
						</div>
					</div>
				</div>
			</div>

			<div class="inquiry">
				<center>
					Make an appointment or give us a call today<br/>to schedule your eye exam.
					<br/>
					<button type="button" class="btn btn-raised btn-default" data-toggle="modal" data-target="#calendar">MAKE AN APPOINTMENT</button>
					<br/>
					<span>(02) 752 7153</span>
				</center>
			</div>
		</div>
	</section>