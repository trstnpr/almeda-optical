	<section class="section-5" id="contact_lens" style="background-image:url('{{ asset('images/contact_lens_bg.jpg') }}');">
		<div class="container section-5-content">
			<div class="row">
				<div class="col-md-8 col-md-offset-4">
					<h1 class="section-title">Contact Lens</h1>
					<div class="underline-000" style="display:inline-block;"></div>
					<p class="text">
						If you are thinking of contact lenses, your doctors have had years of experience and advanced training in fitting even the most difficult cases. Often, people who've been told previously they could not wear contacts have been fit successfully in the office. We participate in FDA supervised studies to investigate new lenses being introduced, thereby giving us access to a greater choice in selecting the appropriate lens for your eyes.
					</p>
				</div>
			</div>
		</div>
	</section>