@extends('app')

@section('stylesheet')
	<link rel="stylesheet" href="{{ elixir('assets/app/views/index.css') }}" />
@stop

@section('content')

	<style>

	</style>

	<!-- .navBar -->
	@include('App::includes.navbar')
	<!-- /.navbar -->

	<!-- .section-1 .banner -->
	@include('App::includes.section-1')
	<!-- /.section-1 .banner -->

	<!-- .section-2 -->
	@include('App::includes.section-2')
	<!-- /.section-2 -->

		<div class="container">
			<div class="row">
				<div class="col-md-2 col-md-offset-5">
					<center><img src="{{ asset('images/glasses.png') }}" class="img-resposive" style="margin-top:-65px;"/></center>
				</div>
			</div>
		</div>


	<!-- .section-3 -->
	@include('App::includes.section-3')
	<!-- /.section-3 -->

		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<center><img src="{{ asset('images/exams.png') }}" class="img-resposive" style="display:block;z-index:1;position:absolute;margin-top:-65px;"/></center>
				</div>
			</div>
		</div>


	<!-- .section-4 -->
	@include('App::includes.section-4')
	<!-- /.section-4 -->

		<div class="container">
			<div class="row">
				<div class="col-md-2 col-md-offset-10">
					<center><img src="{{ asset('images/lens.png') }}" class="img-resposive" style="display:block;z-index:1;position:absolute;margin-top:-65px;float:right;"/></center>
				</div>
			</div>
		</div>

	<!-- .section-5 -->
	@include('App::includes.section-5')
	<!-- /.section-5 -->

	<!-- .section-6 .footer-->
	@include('App::includes.footer')
	<!-- /.section-6 .footer -->

	<!-- Modal Calendar -->
	<div class="modal fade" id="calendar" data-backdrop="static" data-keyboard="false" tabindex="-1" role="dialog">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
					<h4 class="modal-title">Schedule an appointment</h4>
				</div>

				<div class="modal-body calendar"></div>

				<div class="modal-footer">
					<button type="button" class="btn btn-default close-modal" data-dismiss="modal">Close</button>
					<button type="submit" class="btn btn-default form-submit hide">Submit</button>
				</div>
			</div>
		</div>
	</div>

	<!-- appointment form -->
	<div class="modal fade" id="formSched" data-backdrop="static" data-keyboard="false" tabindex="-1" role="dialog">
		<div class="modal-dialog modal-lg">
			<div class="modal-content">
				<form class="scheduleForm">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
						<h4 class="modal-title">Schedule an Appointment</h4>
					</div>
					<div class="modal-body scheduleForm-body">
						<div class="row">
							{!! Form::hidden('type', 'online') !!}
							<div class="form-group label-static col-md-6">
								<label class="form-label">Date</label>
								<input type="text" class="form-control" placeholder="Appoinment date" name="due_date" id="due_date" readonly="readonly" required="required"/>
							</div>
							<div class="form-group label-static col-md-6">
								<label class="form-label">Time</label>
								<select class="form-control" name="due_time" id="due_time" required="required">
									<option selected disabled>-- Start time -- </option>
									<option value="08:00 AM">08:00 AM</option>
									<option value="09:00 AM">09:00 AM</option>
									<option value="10:00 AM">10:00 AM</option>
									<option value="11:00 AM">11:00 AM</option>
									<option value="12:00 PM">12:00 PM</option>
								</select>
							</div>

							<div class="form-group label-static col-md-4">
								<label class="form-label">Last Name</label>
								<input type="text" class="form-control" placeholder="Your last Name" name="last_name" id="lname" required="required"/>
							</div>
							<div class="form-group label-static col-md-4">
								<label class="form-label">First Name</label>
								<input type="text" class="form-control" placeholder="Your first name" name="first_name" id="fname" required="required"/>
							</div>
							<div class="form-group label-static col-md-4">
								<label class="form-label">Middle Name</label>
								<input type="text" class="form-control" placeholder="Your middle Name" name="middle_name" id="mname" required="required"/>
							</div>

							<div class="form-group label-static col-md-8">
								<label class="form-label">Address</label>
								<input type="text" class="form-control" placeholder="House # & Street" name="address_line_1" id="address1" required="required"/>
							</div>
							<div class="form-group label-static col-md-4">
								<label class="form-label">Barangay</label>
								<input type="text" class="form-control" placeholder="Barangay/Subdivision" name="address_line_2" id="address2" required="required"/>
							</div>

							<div class="form-group label-static col-md-5">
								<label class="form-label">City</label>
								<input type="text" class="form-control" placeholder="City" name="address_city" id="city" required="required"/>
							</div>
							<div class="form-group label-static col-md-5">
								<label class="form-label">State</label>
								<input type="text" class="form-control" placeholder="State/Province" name="address_state" id="state" required="required"/>
							</div>
							<div class="form-group label-static col-md-2">
								<label class="form-label">ZIP</label>
								<input type="text" class="form-control" placeholder="Zip code" name="address_zipcode" id="zip"/>
							</div>

							<div class="form-group label-static col-md-6">
								<label class="form-label">Contact</label>
								<input type="text" class="form-control" placeholder="Your contact Number" name="cell_number" id="contact" required="required"/>
							</div>
							<div class="form-group label-static col-md-6">
								<label class="form-label">Email</label>
								<input type="email" class="form-control" placeholder="Your email address" name="email" id="email" required="required"/>
							</div>
							<div class="form-group label-static col-md-12">
								<label class="form-label">Appointment Notes</label>
								<textarea class="form-control" placeholder="Reason of appointment or inquiry" name="notes" id="notes"></textarea>
							</div>
							<div class="form-group label-static col-md-12">
								{!! Recaptcha::render() !!}
							</div>
						</div>
					</div>
					<div class="modal-footer gold">
						<button type="button" class="btn btn-default close-modal" data-dismiss="modal">Cancel</button>
						<button type="submit" class="btn sched-submit">Submit <i class="fa fa-send"></i></button>
					</div>
				</form>
			</div>
		</div>
	</div>



@stop

@section('footer')
	<script src="{{ elixir('assets/app/views/index.js') }}"></script>
	<script src="{{ elixir('assets/jqueryui.js') }}"></script>
@stop
