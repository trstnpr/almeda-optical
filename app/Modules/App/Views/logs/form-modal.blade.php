@if(isset($log))
    {!! Form::model($log, ['method' => 'PUT', 'files' => 'false', 'class' => 'prescription', 'route' => ['logs.update', $log->id_link]]) !!}
@else
    {!! Form::open(['files' => 'false', 'class' => 'prescription', 'route' => ['logs.store']]) !!}
 @endif
        <div class="modal-body">
            <div class="table-responsive">
                <table class="table table-bordered">
                    <thead>
                        <tr>
                            <th class="gold"></th>
                            <th class="gold">Sphere</th>
                            <th class="gold">Cylinder</th>
                            <th class="gold">Axis</th>
                            <th class="gold">Prism</th>
                            <th class="gold">Base</th>
                            <th class="gold">Add</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <th class="gold" scope="row">OD</th>
                            <td>{!! Form::number('left_spherical', 'value', ['class'=> 'form-control']); !!}</td>
                            <td>{!! Form::number('left_cylindrical', 'value', ['class'=> 'form-control']); !!}</td>
                            <td>{!! Form::number('left_axis', 'value', ['class'=> 'form-control']); !!}</td>
                            <td>{!! Form::number('left_prism', 'value', ['class'=> 'form-control']); !!}</td>
                            <td>{!! Form::number('left_base', 'value', ['class'=> 'form-control']); !!}</td>
                            <td>{!! Form::number('left_add', 'value', ['class'=> 'form-control']); !!}</td>
                        </tr>
                        <tr>
                            <th class="gold" scope="row">OS</th>
                            <td>{!! Form::number('right_spherical', 'value', ['class'=> 'form-control']); !!}</td>
                            <td>{!! Form::number('right_cylindrical', 'value', ['class'=> 'form-control']); !!}</td>
                            <td>{!! Form::number('right_axis', 'value', ['class'=> 'form-control']); !!}</td>
                            <td>{!! Form::number('right_prism', 'value', ['class'=> 'form-control']); !!}</td>
                            <td>{!! Form::number('right_base', 'value', ['class'=> 'form-control']); !!}</td>
                            <td>{!! Form::number('right_add', 'value', ['class'=> 'form-control']); !!}</td>
                        </tr>
                        <tr>
                            <th class="gold" scope="row">OU</th>
                            <td colspan="2">{!! Form::number('both_ou', 'value', ['class'=> 'form-control']); !!}</td>
                        </tr>
                    </tbody>
                </table>
                <div class="form-group label-floating">
                    <label class="control-label">Doctor's Remark</label>
                    {!! Form::textarea('notes', null, ['class' => 'form-control', 'rows' => 2]); !!}
                </div>

                <button type="button" class="btn btn-primary btn-raised btn-xs btn-merch" data-toggle="collapse" data-target="#merchandise" aria-expanded="false">
                    <i class="fa fa-plus" id="icon-collapse"></i> Add Merchandise
                </button>
                <div class="collapse" id="merchandise" style="background-color:#eee;padding:18px;">
                    <div class="row">
                        <div class="col-md-3">
                            <h4>Choose Item</h4>
                            <div class="checkbox">
                                <label>
                                    <input type="checkbox" class="cbx" id="lens"> Lenses
                                </label>
                            </div>
                            <div class="checkbox">
                                <label>
                                    <input type="checkbox" class="cbx" id="contact"> Contacts
                                </label>
                            </div>
                            <div class="checkbox">
                                <label>
                                    <input type="checkbox" class="cbx" id="frame"> Frames
                                </label>
                            </div>
                        </div>
                        <div class="col-md-9">
                            <table class="table table-striped table-bordered">
                                <thead>
                                    <tr>   
                                        <th>Item</th>
                                        <th>Description</th>
                                        <th>Price</th>
                                    </tr>
                                </thead>
                                {{-- <tbody>
                                    <tr>
                                        <td>Lenses</td>
                                        <td>Oakley</td>
                                        <td>200.00</td>
                                    </tr>
                                    <tr>
                                        <td>Contacts</td>
                                        <td>Generic</td>
                                        <td>200.00</td>
                                    </tr>
                                    <tr>
                                        <td>Frames</td>
                                        <td>Generic</td>
                                        <td>200.00</td>
                                    </tr>
                                    <tr>
                                        <th colspan="2">Total</th>
                                        <td>500.00</td>
                                    </tr>
                                </tbody> --}}
                                <tbody class="item-list"></tbody>
                                <tr>
                                    <th colspan="2">Total</th>
                                    <th class="total-price"></th>
                                </tr>
                            </table>
                        </div>
                    </div>
                </div>
            </div>

        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-default close-modal" data-dismiss="modal">Close</button>
            <button type="submit" class="btn btn-default form-submit">Submit</button>
        </div>
    {!! Form::close() !!}