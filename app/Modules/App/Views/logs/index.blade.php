@extends('admin')

@section('stylesheet')
<link href="{{ elixir('assets/admin/views/index.css') }}" rel="stylesheet">
@stop

@section('content')
    <div class="container-fluid">

    	<h2 class="content-header">Logs</h2>
    	<ul class="breadcrumb">
			<li><a href="javascript:void(0)">Admin</a></li>
			<li>Logs</li>
		</ul>

		<div class="panel panel-default appointment-logs">
			<div class="panel-heading">Appointment Logs</div>
			<div class="panel-body table-responsive" style="padding:0px;">
				<table class="table table-striped table-hover ">
					<thead>
						<tr>
							<th>#</th>
							<th>Appointment Date & Time</th>
							<th>Type</th>
							<th>Customer</th>
							<th>Doctor</th>
							<th>Actions</th>
						</tr>
					</thead>
					<tbody>
						@if(isset($count))
							@foreach($logs as $log)
							<tr>
								<td>{!! $count++ !!}</td>
								<td>{!! $log->appointment->due_at->format('F d, Y h:i A') !!}</td>
								<td>
									<a href="{!! route('patients.show', $log->patient->id_link) !!}">{!! $log->patient->first_name . ' ' . $log->patient->last_name !!}</a>
								</td>
								<td>{!! $log->patient->cell_number !!}</td>
								<td>{!! $log->appointment->type !!}</td>
								<td><a href="{!! route('logs.show', $appointment->id_link) !!}" class="btn btn-info btn-raised btn-xs">View Details</a></td>
							</tr>
							@endforeach
						@else
							<tr>
								<td class="text-center" colspan="6">
									No Logs
								</td>
							</tr>
						@endif
					</tbody>
				</table>
			</div>
		</div>
	</div>

@stop

@section('footer')
	<script src="{{ elixir('assets/admin/views/index.js') }}"></script>
	<script src="{{ elixir('assets/jqueryui.js') }}"></script>
	<script type="text/javascript">

	</script>
@stop