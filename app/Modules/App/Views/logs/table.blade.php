<table class="table table-striped table-hover">
    <thead>
        <tr>
            <th>Date & Time</th>
            <th>Customer</th>
            <th>Contact</th>
            <th>Location</th>
            <th>Type</th>
            <th>Action</th>
        </tr>
    </thead>
    <tbody class="appointment-data">
        @if(isset($count))
            @foreach($logs as $log)
            <tr>
                <td>{!! $count++ !!}</td>
                <td>{!! $log->appointment->due_at->format('F d, Y h:i A') !!}</td>
                <td>
                    <a href="{!! route('patients.show', $log->patient->id_link) !!}">{!! $log->patient->first_name . ' ' . $log->patient->last_name !!}</a>
                </td>
                <td>{!! $log->patient->cell_number !!}</td>
                <td>{!! $log->appointment->type !!}</td>
                <td><a href="{!! route('logs.show', $appointment->id_link) !!}" class="btn btn-info btn-raised btn-xs">View Details</a></td>
            </tr>
            @endforeach
        @else
            <tr>
                <td class="text-center" colspan="6">
                    No Logs
                </td>
            </tr>
        @endif
    </tbody>
</table>

<div class="text-center">
    {{ pagination_links($logs) }}
</div>