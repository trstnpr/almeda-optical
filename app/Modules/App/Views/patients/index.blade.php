@extends('admin')

@section('stylesheet')

@stop

@section('content')

    <div class="container-fluid">

    	<h2 class="content-header">
    		Patients
		</h2>

    	<ul class="breadcrumb">
			<li><a href="javascript:void(0)">Dashboard</a></li>
			<li>Patient</li>
		</ul>

		<div class="row">
			<div class="col-md-12">
				<div class="panel panel-warning">
					<div class="panel-body table-responsive" style="padding:0px;">
						<table class="table table-striped table-hover ">
							<thead>
								<tr>
									<th>#</th>
									<th>Name</th>
									<th>Contact</th>
									<th>Email</th>
									<th>Actions</th>
								</tr>
							</thead>
							<tbody>
								@if(isset($count))
									@foreach($patients as $patient)
									<tr>
										<td>{!! $count++ !!}</td>
										<td>{!! $patient->full_name !!}</td>
										<td>{!! $patient->cell_number !!}</td>
										<td>{!! $patient->email !!}</td>
										<td><a href="{{ route('patients.show', $patient->id_link) }}" class="btn btn-info btn-raised btn-xs">View Record</a></td>
									</tr>
									@endforeach
								@else
									<tr>
										<td class="text-center" colspan="6">
											No Patients
										</td>
									</tr>
								@endif
							</tbody>
						</table>
						<div class="text-center">
							{{ pagination_links($patients) }}
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
@stop

@section('footer')
	<script src="{{ elixir('assets/jqueryui.js') }}"></script>
	<script src="{{ elixir('assets/admin/views/index.js') }}"></script>
@stop