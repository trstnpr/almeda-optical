@extends('patient')

@section('stylesheet')
<link href="{{ elixir('assets/app/views/patient/show.css') }}" rel="stylesheet">
@stop

@section('content')

    <div class="container">

    	<h2 class="content-header">
    		Gilchrist, Michael Arriana
		</h2>

    	<ul class="breadcrumb">
			<li><a href="javascript:void(0)">Patient</a></li>
			<li>Gilchrist, Michael Arriana</li>
		</ul>

		<div class="well patient-card">
			<div class="form-horizontal">
				<fieldset>
					<div class="center-block">
						<img src="{{ asset('images/logo_2.png') }}" class="img-responsive" />
						<h4>Patient Card</h4>
					</div>
					<hr>
					<div class="form-group">
						<label class="col-sm-2 control-label">Name</label>
						<div class="col-sm-4">
							<input class="form-control" placeholder="Last Name" name="last_name" type="text" value="Gilchrist" readonly="readonly">
						</div>
						<div class="col-sm-4">
							<input class="form-control" placeholder="First Name" name="first_name" type="text" value="Michael" readonly="readonly">
						</div>
						<div class="col-sm-2">
							<input class="form-control" placeholder="Middle Name" name="middle_name" type="text" value="Arriana" readonly="readonly">
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-2 control-label">Address</label>
						<div class="col-sm-6">
							<input class="form-control" name="address_line_1" type="text" value="#123 Maes Street" readonly="readonly">
						</div>
						<div class="col-sm-4">
							<input class="form-control" name="address_line_2" type="text" value="Barangay" readonly="readonly">
						</div>
					</div>
					<div class="form-group">
						<div class="col-md-offset-2 col-sm-4">
							<input class="form-control" name="address_city" type="text" value="Makati City" readonly="readonly">
						</div>
						<div class="col-sm-4">
							<input class="form-control" name="address_state" type="text" value="Metro Manila" readonly="readonly">
						</div>
						<div class="col-sm-2">
							<input class="form-control" name="address_zipcode" type="text" value="2222" readonly="readonly">
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-2 control-label">Cell Number</label>
						<div class="col-sm-10">
							<input class="form-control" name="cell_number" type="text" value="09876543211" readonly="readonly">
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-2 control-label">Email</label>
						<div class="col-sm-10">
							<input class="form-control" name="email" type="email" value="louievillena1205@gmail.com" readonly="readonly">
						</div>
					</div>
				</fieldset>
			</div>
		</div>

		<div class="fixed-action-btn" style="bottom: 45px; right: 24px;">
			<button class="btn btn-primary btn-raised btn-fab" data-toggle="modal" data-target="#logs"><i class="material-icons">list</i></button>
		</div>

		<!-- Individual Logs -->
		<div class="modal fade" id="logs" data-backdrop="static" data-keyboard="false" tabindex="-1" role="dialog">
			<div class="modal-dialog modal-lg">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
						<h4 class="modal-title">Logs</h4>
					</div>
					<div class="modal-body table-responsive" style="padding:0;">
						<table class="table table-striped table-hover ">
							<thead>
								<tr>
									<th>#</th>
									<th>Date &amp; Time</th>
									<th>Details</th>
									<th>Notes</th>
									<th>Type</th>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td>1</td>
									<td>September 07, 2016 09:00 AM</td>
									<td>
										<table class="table table-bordered">
											<thead>
												<tr>
													<th></th>
													<th>Sphere</th>
													<th>Cylinder</th>
													<th>Axis</th>
													<th>Prism</th>
													<th>Base</th>
													<th>Add</th>
												</tr>
											</thead>
											<tbody>
												<tr>
													<th scope="row">OD</th>
													<td>1</td>
													<td>2</td>
													<td>3</td>
													<td>4</td>
													<td>5</td>
													<td>6</td>
												</tr>
												<tr>
													<th scope="row">OS</th>
													<td>7</td>
													<td>8</td>
													<td>9</td>
													<td>10</td>
													<td>11</td>
													<td>12</td>
												</tr>
												<tr>
													<th scope="row">OU</th>
													<td>11</td>
													<td>12</td>
												</tr>
											</tbody>
										</table>
									</td>
									<td>Lorem ipsum dolor sit amet</td>
									<td>Online</td>
								</tr>
								<tr>
									<td>1</td>
									<td>September 07, 2016 09:00 AM</td>
									<td>
										<table class="table table-bordered">
											<thead>
												<tr>
													<th></th>
													<th>Sphere</th>
													<th>Cylinder</th>
													<th>Axis</th>
													<th>Prism</th>
													<th>Base</th>
													<th>Add</th>
												</tr>
											</thead>
											<tbody>
												<tr>
													<th scope="row">OD</th>
													<td>1</td>
													<td>2</td>
													<td>3</td>
													<td>4</td>
													<td>5</td>
													<td>6</td>
												</tr>
												<tr>
													<th scope="row">OS</th>
													<td>7</td>
													<td>8</td>
													<td>9</td>
													<td>10</td>
													<td>11</td>
													<td>12</td>
												</tr>
												<tr>
													<th scope="row">OU</th>
													<td>11</td>
													<td>12</td>
												</tr>
											</tbody>
										</table>
									</td>
									<td>Lorem ipsum dolor sit amet</td>
									<td>Online</td>
								</tr>
								<tr>
									<td>1</td>
									<td>September 07, 2016 09:00 AM</td>
									<td>
										<table class="table table-bordered">
											<thead>
												<tr>
													<th></th>
													<th>Sphere</th>
													<th>Cylinder</th>
													<th>Axis</th>
													<th>Prism</th>
													<th>Base</th>
													<th>Add</th>
												</tr>
											</thead>
											<tbody>
												<tr>
													<th scope="row">OD</th>
													<td>1</td>
													<td>2</td>
													<td>3</td>
													<td>4</td>
													<td>5</td>
													<td>6</td>
												</tr>
												<tr>
													<th scope="row">OS</th>
													<td>7</td>
													<td>8</td>
													<td>9</td>
													<td>10</td>
													<td>11</td>
													<td>12</td>
												</tr>
												<tr>
													<th scope="row">OU</th>
													<td>11</td>
													<td>12</td>
												</tr>
											</tbody>
										</table>
									</td>
									<td>Lorem ipsum dolor sit amet</td>
									<td>Online</td>
								</tr>
							</tbody>
						</table>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-default close-modal" data-dismiss="modal">Close</button>
						<button type="submit" class="btn btn-default form-submit hide">Submit</button>
					</div>
				</div>
			</div>
		</div>

	</div>

@stop

@section('footer')

@stop