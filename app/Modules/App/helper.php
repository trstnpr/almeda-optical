<?php
/**
 * Check if a valid date
 * @param  string|mixed $date data to be checked
 * @return boolean       true|false
 */
function check_if_valid_date($date)
{
    return date_parse($date)['error_count'] == 0 && checkdate(date_parse($date)['month'], date_parse($date)['day'], date_parse($date)['year']);
}
