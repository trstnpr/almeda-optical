<?php
// FakeId bindings
Route::obfuscate('appointments', 'App\Modules\App\Models\Appointment');
Route::obfuscate('logs', 'App\Modules\App\Models\Log');
Route::obfuscate('patients', 'App\Modules\App\Models\Patient');
Route::obfuscate('items', 'App\Modules\App\Models\Item');
Route::obfuscate('orders', 'App\Modules\App\Models\Order');

Route::group(['module' => 'App', 'namespace' => 'App\Modules\App\Controllers'], function() {
    Route::get('app', 'AppController@index');
    Route::post('appointments',['as' => 'appointments.store', 'uses' => 'AppointmentController@store']);

    /* Test */
    Route::get('patient/user', 'PatientController@patientUser');

    Route::group(['prefix' => 'admin', 'middleware' => ['web', 'auth']], function() {
        Route::resource('appointments', 'AppointmentController', [
            'except' => ['store'],
            'names' => [
                'index'     => 'appointments.index',
                'create'    => 'appointments.create',
                'show'      => 'appointments.show',
                'edit'      => 'appointments.edit',
                'update'    => 'appointments.update',
                'destroy'   => 'appointments.delete'
            ]
        ]);

        Route::resource('logs', 'LogController', [
            'names' => [
                'index'     => 'logs.index',
                'create'    => 'logs.create',
                'store'     => 'logs.store',
                'show'      => 'logs.show',
                'edit'      => 'logs.edit',
                'update'    => 'logs.update',
                'destroy'   => 'logs.delete'
            ]
        ]);

        Route::resource('patients', 'PatientController', [
            'names' => [
                'index'     => 'patients.index',
                'create'    => 'patients.create',
                'store'     => 'patients.store',
                'show'      => 'patients.show',
                'edit'      => 'patients.edit',
                'update'    => 'patients.update',
                'destroy'   => 'patients.delete'
            ]
        ]);

        Route::resource('items', 'ItemController', [
            'names' => [
                'index'     => 'items.index',
                'create'    => 'items.create',
                'store'     => 'items.store',
                'show'      => 'items.show',
                'edit'      => 'items.edit',
                'update'    => 'items.update',
                'destroy'   => 'items.delete'
            ]
        ]);

        Route::resource('orders', 'OrderController', [
            'names' => [
                'index'     => 'orders.index',
                'create'    => 'orders.create',
                'store'     => 'orders.store',
                'show'      => 'orders.show',
                'edit'      => 'orders.edit',
                'update'    => 'orders.update',
                'destroy'   => 'orders.delete'
            ]
        ]);
    });
});
