<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Jenssegers\Optimus\Optimus;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class ObfuscatorServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        // Register Optimus
        $this->app->singleton(Optimus::class, function ($app) {
            return new Optimus(
                $app['config']['obfuscate.prime'],
                $app['config']['obfuscate.inverse'],
                $app['config']['obfuscate.random']
            );
        });
        $this->app->alias(Optimus::class, 'Obfuscator');

        $this->registerRouterMacro();
    }

    /**
     * Register the custom router macro.
     */
    protected function registerRouterMacro()
    {
        $this->app['router']->macro('obfuscate', function ($key, $class, Closure $callback = null) {
            $this->bind($key, function ($value) use ($key, $class, $callback) {
                if (is_null($value)) {
                    return;
                }

                // For model binders, we will attempt to retrieve the models using the first
                // method on the model instance. If we cannot retrieve the models we'll
                // throw a not found exception otherwise we will return the instance.
                $instance = $this->container->make($class);

                if (in_array('App\Traits\ObfuscatorTrait', class_uses($class))) {
                    return $this->container->make('Obfuscator')->decode($value);
                }

                // If a callback was supplied to the method we will call that to determine
                // what we should do when the model is not found. This just gives these
                // developer a little greater flexibility to decide what will happen.
                if ($callback instanceof Closure) {
                    return call_user_func($callback, $value);
                }

                throw new NotFoundHttpException;
            });
        });
    }
}
