<?php
namespace App\Traits;

use Illuminate\Support\Facades\App;

trait ObfuscatorTrait
{
    /**
     * Get the value of the model's route key.
     *
     * @return mixed
     */
    public function getRouteKey()
    {
        return App::make('Obfuscator')->encode($this->getKey());
    }

    public function getIdLinkAttribute($value)
    {
        return App::make('Obfuscator')->encode($this->attributes['id']);
    }
}

