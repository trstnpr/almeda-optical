server "13.76.47.147", port: 22, roles: [:app], :primary => true, user: 'deployer'
set :application => "dev.op.devhub.ph"
set :branch, "pre-release"
set :deploy_to, "/var/www/vhosts/localhost.localdomain/dev.op.devhub.ph"