<?php

return[
    'enable' => [
        'App',
        'Admin'
    ],
    'models' => [
        'patient'       => 'App\Modules\App\Models\Patient',
        'appointment'   => 'App\Modules\App\Models\Appointment',
        'log'           => 'App\Modules\App\Models\Log',
        'item'          => 'App\Modules\App\Models\Item',
        'order'         => 'App\Modules\App\Models\Order',
        'user'          => 'App\Modules\Admin\Models\User',
        'role'          => 'App\Modules\Admin\Models\Role',
        'permission'    => 'App\Modules\Admin\Models\Permission'
    ],
    'perpage' => 10
];
