<?php

return [
    'prime'   => env('OPTIMUS_PRIME', 961748927),
    'inverse' => env('OPTIMUS_INVERSE', 1430310975),
    'random'  => env('OPTIMUS_RANDOM', 620464665),
];
