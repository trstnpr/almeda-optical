<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePatientsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('patients', function (Blueprint $table) {
            $table->increments('id')->index();
            $table->string('last_name');
            $table->string('first_name');
            $table->string('middle_name');
            $table->string('address_line_1');
            $table->string('address_line_2');
            $table->string('address_city');
            $table->string('address_state');
            $table->string('address_zipcode');
            $table->string('cell_number');
            $table->string('email');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('patients');
    }
}
