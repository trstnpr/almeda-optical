<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLogsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('logs', function (Blueprint $table) {
            $table->increments('id')->index();
            $table->double('left_spherical');
            $table->double('left_cylindrical');
            $table->integer('left_axis');
            $table->double('left_prism');
            $table->char('left_base', 2);
            $table->double('left_add');
            $table->double('right_spherical');
            $table->double('right_cylindrical');
            $table->integer('right_axis');
            $table->double('right_prism');
            $table->char('right_base', 2);
            $table->double('right_add');
            $table->double('both_ou');
            $table->string('notes');
            $table->timestamps();
            $table->integer('appointment_id')->unsigned()->nullable();
            $table->integer('patient_id')->unsigned()->nullable();
            $table->integer('user_id')->unsigned()->nullable();

            $table->foreign('appointment_id')
                ->references('id')
                ->on('appointments');

            $table->foreign('patient_id')
                ->references('id')
                ->on('patients');

            $table->foreign('user_id')
                ->references('id')
                ->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('logs');
    }
}
