<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateItemOrderTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('item_order', function (Blueprint $table) {
            $table->integer('item_id')->unsigned()->index();
            $table->integer('order_id')->unsigned()->index();

            $table->foreign('item_id')
                ->references('id')
                ->on('items');

            $table->foreign('order_id')
                ->references('id')
                ->on('orders')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('item_order');
    }
}
