<?php

use App\Modules\App\Models\Appointment;
use App\Modules\App\Models\Patient;
use Carbon\Carbon;
use Illuminate\Database\Seeder;

class AppointmentsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create();

        for($i = 0; $i < 50; $i++) {
            Patient::create([
                'last_name' => $faker->lastName,
                'first_name' => $faker->firstName,
                'middle_name' => $faker->lastName,
                'email' => $faker->email,
                'cell_number' => $faker->e164PhoneNumber,
                'address_line_1' => $faker->buildingNumber . ' ' . $faker->streetName,
                'address_line_2' => $faker->secondaryAddress,
                'address_city' => $faker->city,
                'address_state' => $faker->state,
                'address_zipcode' => $faker->postcode
            ])->appointment()
            ->create([
                'type' => 'online',
                'notes' => $faker->paragraph(),
                'due_at' => Carbon::parse($faker->unique()->dateTimeThisYear()->format('Y-m-d H:i:s'))
            ]);
        }
    }
}
