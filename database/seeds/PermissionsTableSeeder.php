<?php

use App\Modules\Admin\Models\Permission;
use Illuminate\Database\Seeder;

class PermissionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $permissions = [
            ['create-roles', 'Create Roles'],
            ['read-roles', 'Read Roles'],
            ['update-roles', 'Update Roles'],
            ['delete-roles', 'Delete Roles'],
            ['create-users', 'Create Users'],
            ['read-users', 'Read Users'],
            ['update-users', 'Update Users'],
            ['delete-users', 'Delete Users'],
            ['create-appointments', 'Create Appointments'],
            ['read-appointments', 'Read Appointments'],
            ['update-appointments', 'Update Appointments'],
            ['delete-appointments', 'Delete Appointments'],
            ['create-logs', 'Create Logs'],
            ['read-logs', 'Read Logs'],
            ['update-logs', 'Update Logs'],
            ['delete-logs', 'Delete Logs'],
            ['create-items', 'Create Items'],
            ['read-items', 'Read Items'],
            ['update-items', 'Update Items'],
            ['delete-items', 'Delete Items'],
            ['create-orders', 'Create Orders'],
            ['read-orders', 'Read Orders'],
            ['update-orders', 'Update Orders'],
            ['delete-orders', 'Delete Orders'],
        ];

        foreach($permissions as $permission) {
            Permission::create([
                'name' => $permission[0],
                'display_name' => $permission[1],
                'description' => $permission[1]
            ]);
        }
    }
}
