<?php

use App\Modules\Admin\Models\Role;
use App\Modules\Admin\Models\Permission;
use Illuminate\Database\Seeder;

class RolesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $i = 1;
        $permissionDoctor = [9, 10, 11, 12, 13, 14, 15, 16];
        $permissionClerk = [17, 18, 19, 20, 21, 22, 23, 24];

        $role = Role::create([
            'name' => 'Site Administrator'
        ]);

        for($i = 1; $i <= 24; $i++) {
            $role->attachPermission($i);
        }

        $role = Role::create([
            'name' => 'Backend Support'
        ]);

        for($i = 1; $i <= 24; $i++) {
            $role->attachPermission($i);
        }

        $role = Role::create([
            'name' => 'Doctor'
        ]);

        foreach($permissionDoctor as $permission) {
            $role->attachPermission($permission);
        }

        $role = Role::create([
            'name' => 'Clerk'
        ]);

        foreach($permissionClerk as $permission) {
            $role->attachPermission($permission);
        }

    }
}
