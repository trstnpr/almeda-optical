<?php

use App\Modules\Admin\Models\User;
use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = User::create([
            'username'      => 'admin',
            'email'         => 'admin@op.devhub.ph',
            'password'      => '1234qwer',
            'last_name'     => 'Admin',
            'first_name'    => 'Super',
            'middle_name'   => 'Duper',
            'avatar'        => '',
        ]);

        $user->roles()->attach(1);
    }
}
