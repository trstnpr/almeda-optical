gulp = require('gulp');

var elixir = require('laravel-elixir');
require('laravel-elixir-bowerbundle');

var AWS = require('aws-sdk');
var config = new AWS.Config().loadFromPath('upload.json');
var s3 = require('gulp-s3-upload')(config);

var bower_path = "./bower_components";
var paths = {
  'bootstrap'  : bower_path + "/bootstrap-sass/assets"
};
elixir.config.sourcemaps = false;

/*
 |--------------------------------------------------------------------------
 | Elixir Asset Management
 |--------------------------------------------------------------------------
 |
 | Elixir provides a clean, fluent API for defining some basic Gulp tasks
 | for your Laravel application. By default, we are compiling the Sass
 | file for our application, as well as publishing vendor resources.
 |
 */

elixir(function(mix) {
    mix.bower(['jquery', 'bootstrap', 'bootstrap-material-design', 'snackbarjs'], 'public/assets/bundle.js');
    mix.copy('bower_components/font-awesome/fonts', 'public/build/assets/fonts');
    mix.copy('bower_components/bootstrap-sass/assets/fonts/bootstrap', 'public/build/assets/fonts');
    mix.copy('bower_components/op-fonts/fonts', 'public/build/assets/fonts');
    mix.copy('bower_components/material-design-icons/iconfont', 'public/build/assets/fonts');
    mix.scripts(['resources/assets/js/jqueryui.js'], 'public/assets/jqueryui.js', './');

    // App
    mix.scripts(['public/assets/bundle.js','resources/assets/js/app/main.js'], 'public/assets/app/app.js', './');
    mix.sass(['app/main.scss', './bower_components/snackbarjs/dist/snackbar.css', './bower_components/snackbarjs/themes-css/material.css'], 'public/assets/app/app.css', {includePaths: [paths.bootstrap + '/stylesheets']});
    mix.sass('app/views/index.scss', 'public/assets/app/views/index.css');
    mix.sass('app/views/patient/show.scss', 'public/assets/app/views/patient/show.css');
    mix.copy('resources/assets/js/app/views', 'public/assets/app/views');

    // Admin
    mix.scripts(['public/assets/bundle.js','resources/assets/js/admin/main.js'], 'public/assets/admin/admin.js', './');
    mix.sass(['admin/main.scss', './bower_components/snackbarjs/dist/snackbar.css', './bower_components/snackbarjs/themes-css/material.css'], 'public/assets/admin/admin.css', {includePaths: [paths.bootstrap + '/stylesheets']});
    mix.sass('admin/views/index.scss', 'public/assets/admin/views/index.css');
    mix.sass('admin/views/login/index.scss', 'public/assets/admin/views/login/index.css');
    mix.copy('resources/assets/js/admin/views', 'public/assets/admin/views');

    // Versioning & Cache Busting
    mix.version([
        'public/assets/jqueryui.js',
        'public/assets/app/app.css',
        'public/assets/app/app.js',
        'public/assets/app/views/index.css',
        'public/assets/app/views/index.js',
        'public/assets/app/views/*',
        'public/assets/admin/admin.css',
        'public/assets/admin/admin.js',
        'public/assets/admin/views/index.css',
        'public/assets/admin/views/index.js',
        'public/assets/admin/views/*'
    ]);

    //Upload to S3
    // mix.task('upload');
});

/**
 * Define a new gulp task upload that uploads current static assets to S3.
 * Run gulp --production first before running this task.
 */
gulp.task('upload', function() {
    gulp.src('public/build/**')
        .pipe(s3({
            Bucket: '/dev-op/build',
            ACL:    'public-read'
        }, {
            maxRetries: 50
        }))
    ;
    // gulp.src('public/images/**')
    //     .pipe(s3({
    //         Bucket: '/op-dev/images',
    //         ACL:    'public-read'
    //     }, {
    //         maxRetries: 50
    //     }))
    // ;
});
