Almeda Optical
==============================

### Setup ###

##### For local development #####

> https://www.npmjs.com/package/maildev
> `npm install maildev -g`

> https://www.npmjs.com/package/s3rver
> `npm install s3rver -g`

###### Configuration #######
**For Linux** `/etc/hosts` **For Windows** `C:/Windows/System32/drivers/etc/hosts`
```
    127.0.0.1       s3server.dev
    127.0.0.1       dev-op.s3server.dev
    127.0.0.1       app.mailserver.dev
```

**Virtual hosts Apache**
```
#!apacheconf
<VirtualHost *:80>
    ServerName s3server.dev
    ProxyPreserveHost On

    <Proxy *>
        Order allow,deny
        Allow from all
    </Proxy>

    ProxyPass / http://s3server.dev:4568/
    ProxyPassReverse / http://s3server.dev:4568/
</VirtualHost>

<VirtualHost *:80>
    ServerName dev-op.s3server.dev
    ProxyPreserveHost On

    <Proxy *>
        Order allow,deny
        Allow from all
    </Proxy>

    ProxyPass / http://s3server.dev:4568/dev-op/
    ProxyPassReverse / http://s3server.dev:4568/dev-op/
</VirtualHost>

<VirtualHost *:80>
    ServerName app.mailserver.dev
    ProxyPreserveHost On

    <Proxy *>
        Order allow,deny
        Allow from all
    </Proxy>

    ProxyPass / http://localhost:1080/
    ProxyPassReverse / http://localhost:1080/
</VirtualHost>
```

**Apache** `httpd.conf`
Uncomment the following:
```
#!apacheconf
LoadModule proxy_module modules/mod_proxy.so
LoadModule proxy_http_module modules/mod_proxy_http.so
```

**Email config**
```
#!env
MAIL_DRIVER=smtp
MAIL_HOST=localhost
MAIL_PORT=1025
MAIL_USERNAME=
MAIL_PASSWORD=
MAIL_ENCRYPTION=null
```

**S3 Configuration**
```
#!env
S3_ACCESS_KEY_ID=
S3_SECRET_ACCESS_KEY=
S3_ENDPOINT=s3server.dev
S3_BUCKET=dev-op
S3_BUCKET_LINK=//dev-op.s3server.dev
```

### Running the dummy servers ###
*Note: Run on different instances of terminals*
> `maildev --ip=localhost`

> `s3rver -h s3server.dev -d path/to/file/storage`

### Accessing intercepted mails ###
http://app.mailserver.dev
* * *
###### Directory Structure #######
* **app**
    - **Modules**
        + **Sample**
            * **Views**
                - **subroute folder**
                    + **partials**
                    + `index.blade.php`
                - **partials**
                - `index.blade.php`
* **resources**
    - **assets**
        + js
            * **sample**
                - **views**
                    + **subroute folder**
                        * *subroute specific js here*
                    + `index.js` - *main route js*
                - `main.js` - *module-wide js*
            * *app-wide js includes here*
        + scss
            * **sample**
                - **views**
                    + **subroute folder**
                        * *subroute specific scss here*
                    + `index.scss` - *main route scss*
                - `main.scss` - *module-wide scss*
            * *app-wide scss includes here*
    - **lang**
    - **views**
        + **errors**
        + **vendor**
        + *module master layout files here*
        + `*module name*.blade.php`
