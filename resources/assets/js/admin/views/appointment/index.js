$(document).ready(function() {
    // Calendar Onload
    $('.calendar').datepicker({
        inline: true,
        firstDay: 0,
        showOtherMonths: true,
        dayNamesMin: ['Sn', 'M', 'T', 'W', 'TH', 'F', 'St'],
        onSelect: function(dateText, inst) {
            show_form(dateText);
        }
    });
    function show_form(date) {
        $('#calendar').modal('hide');
        $('.form-appointment').show();
        $('.form-appointment #due_date').val(date);
        $("html, body").animate({
            scrollTop: $(".form-appointment").offset().top
        }, 1000);
    }
    $('.sched-cancel').click(function() {
        $('.appointment-form')[0].reset();
        $('.form-appointment').hide();
    });

    $('.appointment-form').on('submit', function(e) {
        e.preventDefault();
        console.dir(new FormData(this));
        $.ajax({
            url: '/appointments',
            type: 'POST',
            data: new FormData(this),
            contentType: false,
            cache: false,
            processData: false,
            beforeSend: function() { $('.sched-submit').html('Processing <i class="fa fa-circle-o-notch fa-spin fa-fw"></i>'); },
            success: function(data) {
                var msg = JSON.parse(data);
                if(msg.status == 'success') {
                    $('.sched-submit').html('Submit <i class="fa fa-send">');
                    $('.appointment-form')[0].reset();
                    location.reload();
                } else {
                    $('.sched-submit').html('Submit <i class="fa fa-send">');
                    console.log(msg.message);
                }
            }
        });
    });
});