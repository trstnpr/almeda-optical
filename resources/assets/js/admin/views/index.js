$('#calendar-schedule').datepicker({
    inline: true,
    firstDay: 0,
    showOtherMonths: true,
    dayNamesMin: ['Sn', 'M', 'T', 'W', 'TH', 'F', 'St'],
    onSelect: function(dateText, inst) {
        open_schedule(dateText);
    }
});

function open_schedule(dateText) {

    var calendar = new Array('January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December');
    var d = new Date(dateText);
    var day = d.getDate();
    var month = d.getMonth();
    var year = d.getFullYear();
    var date = calendar[month]+' '+day+", "+year;

    $.get('admin/appointments', { q: dateText }, function(data) {
        var rv = JSON.parse(data);
        if(rv.status == 'success') {
            $('#schedule_view').modal('show');
            $('#schedule_view .modal-title').html(date);
            $(document).on('click', '.pagination a', function (event) {
                event.preventDefault();
                var page = $(this).attr('href');
                $.get(page, null, function(data) {
                    var rv = JSON.parse(data);
                    if(rv.status == 'success') {
                        $('#schedule_view .modal-body.table-responsive').html(rv.data);
                    } else {
                        console.log(rv.data);
                    }
                });
            });
            $('#schedule_view .modal-body.table-responsive').html(rv.data);
        } else {
            console.log(rv.message);
        }
    });
}
