$(document).ready(function() {
	//navbar
	var scroll_start = 0;
	var startchange = $('.startchange');
	var offset = startchange.offset();
	if (startchange.length) {
		$(document).scroll(function() {
			scroll_start = $(this).scrollTop();
			if(scroll_start > offset.top) {
				$('.navbar').css('background-color', '#ebcb80');
			} else {
				$('.navbar').css('background-color', 'rgba(0, 0, 0, 0.1)!important');
			}
		});
	}

	var scroll_start_logo = 0;
	var logo_change = $('#startLogo');
	var logo = logo_change.offset();
	if (logo_change.length) {
		$(document).scroll(function() {
			scroll_start_logo = $(this).scrollTop();
			if(scroll_start_logo > logo.top) {
				$('.navbar-brand').show();
			} else {
				$('.navbar-brand').hide();
			}
		});
	}

	$(".menu-scroll").on("click", function(e) {
		e.preventDefault();
		$("body, html").animate({
			scrollTop: $( $(this).attr('href')).offset().top
		}, 600);
	});

	// Calendar Onload
    $('.calendar').datepicker({
        inline: true,
        firstDay: 0,
        showOtherMonths: true,
        dayNamesMin: ['Sn', 'M', 'T', 'W', 'TH', 'F', 'St'],
        onSelect: function(dateText, inst) {
			open_schedule(dateText);
	    }
    });
    function open_schedule(date) {
    	$('#calendar').modal('hide');
    	$('#formSched #due_date').val(date);
    	$('#formSched').modal('show');
    }

    $(document).ready(function() {
        $('.scheduleForm').on('submit', function(e) {
            e.preventDefault();
            $.ajax({
                url: 'appointments',
                type: 'POST',
                data: new FormData(this),
                contentType: false,
                cache: false,
                processData: false,
                beforeSend: function() { $('.sched-submit').html('Processing <i class="fa fa-circle-o-notch fa-spin fa-fw"></i>'); },
                error: function(data) {
                    $.each(data.responseJSON, function(key, value) {
                        var options =  {
                            content: value[0], // text of the snackbar
                            style: 'toast error', // add a custom class to your snackbar
                            timeout: 5000 // time in milliseconds after the snackbar autohides, 0 is disabled
                        }
                        $.snackbar(options);
                    });
                    $('.sched-submit').html('Submit <i class="fa fa-send">');
                },
                success: function(data) {
                    var msg = JSON.parse(data);
                    if(msg.status == 'success') {
                        $('.sched-submit').html('Submit <i class="fa fa-send">');
                        $('.scheduleForm')[0].reset();
                        grecaptcha.reset();
                        $('#formSched').modal('hide');
                        var options =  {
                            content: msg.message, // text of the snackbar
                            style: 'toast notice', // add a custom class to your snackbar
                            timeout: 5000 // time in milliseconds after the snackbar autohides, 0 is disabled
                        }
                        $.snackbar(options);
                    } else {
                        $('.sched-submit').html('Submit <i class="fa fa-send">');
                    }
                }
            });
        });
    });
});