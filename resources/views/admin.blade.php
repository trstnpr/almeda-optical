<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="initial-scale=1, width=device-width">
        <title>Almeda Optical</title>
        <link rel="apple-touch-icon" sizes="180x180" href="/apple-touch-icon.png">
        <link rel="icon" type="image/png" href="/favicon-32x32.png" sizes="32x32">
        <link rel="icon" type="image/png" href="/favicon-16x16.png" sizes="16x16">
        <link rel="manifest" href="/manifest.json">
        <link rel="mask-icon" href="/safari-pinned-tab.svg" color="#5bbad5">
        <meta name="theme-color" content="#ffffff">
        <link href="{{ elixir('assets/admin/admin.css') }}" rel="stylesheet">
        
        @yield('stylesheet')
    </head>
    <body>
        <div id="wrapper">
            @include('Admin::partials.sidebar')
            <div id="page-content-wrapper">
                @include('Admin::partials.flash')
                @include('Admin::partials.navbar')
                @yield('content')
            </div>
        </div>

        <script src="{{ elixir('assets/admin/admin.js') }}"></script>
        @yield('footer')
    </body>
</html>
