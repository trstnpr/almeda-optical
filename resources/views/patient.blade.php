<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="initial-scale=1, width=device-width">
        <title>Almeda Optical</title>
        <link rel="apple-touch-icon" sizes="180x180" href="/apple-touch-icon.png">
        <link rel="icon" type="image/png" href="/favicon-32x32.png" sizes="32x32">
        <link rel="icon" type="image/png" href="/favicon-16x16.png" sizes="16x16">
        <link rel="manifest" href="/manifest.json">
        <link rel="mask-icon" href="/safari-pinned-tab.svg" color="#5bbad5">
        <meta name="theme-color" content="#ffffff">
        <link href="{{ elixir('assets/admin/admin.css') }}" rel="stylesheet">
        
        @yield('stylesheet')
    </head>
    <body>
        <div id="patient-wrap">
            <div class="navbar navbar-default">
                <div class="container">
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-responsive-collapse">
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                        <a class="navbar-brand" href="javascript:void(0)"><span>Almeda Optical</span></a>
                    </div>
                    <div class="navbar-collapse collapse navbar-responsive-collapse">
                        <ul class="nav navbar-nav navbar-right">
                            <li class="dropdown">
                                <a href="bootstrap-elements.html" data-target="#" class="dropdown-toggle" data-toggle="dropdown">
                                    <i class="fa fa-user"></i> Patient Name <b class="caret"></b>
                                </a>
                                <ul class="dropdown-menu">
                                    <li><a href="javascript:void(0)">Logout <i class="material-icons pull-right">exit_to_app</i></a></li>
                                </ul>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>

            <div id="page-content-wrapper">
                @yield('content')
            </div>
        </div>
        <script src="{{ elixir('assets/admin/admin.js') }}"></script>
        @yield('footer')
    </body>
</html>
